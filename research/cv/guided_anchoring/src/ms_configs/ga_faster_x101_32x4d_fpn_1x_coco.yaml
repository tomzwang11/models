# Builtin Configurations(DO NOT CHANGE THESE CONFIGURATIONS unless you know exactly what you are doing)
enable_modelarts: False
data_url: ""
train_url: "/cache/data/guided_anchoring_models"
checkpoint_url: ""
data_path: "/cache/data"
output_path: "/cache/train"
load_path: "/cache/checkpoint_path"
enable_profiling: False

train_outputs: '/data/guided_anchoring_models'
brief: 'gpu-1_1024x1024'
device_target: GPU
# ==============================================================================

# optimizer
opt_type: "sgd"
lr: 0.02
momentum: 0.9
weight_decay: 0.0001
warmup_step: 500
warmup_ratio: 0.001
lr_steps: [8, 11]
lr_type: "multistep"
grad_clip: 0


# train
num_gts: 100
batch_size: 4
test_batch_size: 1
loss_scale: 256
epoch_size: 12
run_eval: True
eval_every: 1
enable_graph_kernel: False
finetune: False
datasink: False
pre_trained: ''

#distribution training
run_distribute: False
device_id: 0
device_num: 1
rank_id: 0

# Number of threads used to process the dataset in parallel
num_parallel_workers: 4
# Parallelize Python operations with multiple worker processes
python_multiprocessing: False

# dataset setting
train_data_type: 'coco'
val_data_type: 'mindrecord'
train_dataset: '/data/coco-2017/train'
val_dataset: '/data/mindrecord_eval'
coco_classes: ['background', 'person', 'bicycle', 'car', 'motorcycle',
               'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']
num_classes: 81
train_dataset_num: 0
train_dataset_divider: 0

# images
img_width: 1024
img_height: 1024
divider: 32
img_mean: [123.675, 116.28, 103.53]
img_std: [58.395, 57.12, 57.375]
to_rgb: True
keep_ratio: False

# augmentation
flip_ratio: 0.5
expand_ratio: 0.0

# callbacks
save_every: 100
keep_checkpoint_max: 5
keep_best_checkpoints_max: 5

# ==============================================================================
model:
  backbone:
    pretrained: None
    base_width: 4
    depth: 101
    frozen_stages: 1
    groups: 32
    init_cfg:
      checkpoint: open-mmlab://resnext101_32x4d
      type: Pretrained
    norm_cfg:
      requires_grad: true
      type: BN
    norm_eval: true
    num_stages: 4
    out_indices: !!python/tuple
    - 0
    - 1
    - 2
    - 3
    style: pytorch
    type: ResNeXt
  neck:
    in_channels:
    - 256
    - 512
    - 1024
    - 2048
    num_outs: 5
    out_channels: 256
    type: FPN
  roi_head:
    bbox_head:
      bbox_coder:
        target_means:
        - 0.0
        - 0.0
        - 0.0
        - 0.0
        target_stds:
        - 0.05
        - 0.05
        - 0.1
        - 0.1
        type: DeltaXYWHBBoxCoder
      fc_out_channels: 1024
      in_channels: 256
      loss_bbox:
        loss_weight: 1.0
        type: L1Loss
      loss_cls:
        loss_weight: 1.0
        type: CrossEntropyLoss
        use_sigmoid: false
      num_classes: 80
      reg_class_agnostic: false
      roi_feat_size: 7
      type: Shared2FCBBoxHead
    bbox_roi_extractor:
      featmap_strides:
      - 4
      - 8
      - 16
      - 32
      out_channels: 256
      roi_layer:
        output_size: 7
        sampling_ratio: 0
        type: RoIAlign
      type: SingleRoIExtractor
    type: StandardRoIHead
  rpn_head:
    anchor_coder:
      target_means:
      - 0.0
      - 0.0
      - 0.0
      - 0.0
      target_stds:
      - 0.07
      - 0.07
      - 0.14
      - 0.14
      type: DeltaXYWHBBoxCoder
    approx_anchor_generator:
      octave_base_scale: 8
      ratios:
      - 0.5
      - 1.0
      - 2.0
      scales_per_octave: 3
      strides:
      - 4
      - 8
      - 16
      - 32
      - 64
      type: AnchorGenerator
    bbox_coder:
      target_means:
      - 0.0
      - 0.0
      - 0.0
      - 0.0
      target_stds:
      - 0.07
      - 0.07
      - 0.11
      - 0.11
      type: DeltaXYWHBBoxCoder
    feat_channels: 256
    in_channels: 256
    loc_filter_thr: 0.01
    loss_bbox:
      beta: 1.0
      loss_weight: 1.0
      type: SmoothL1Loss
    loss_cls:
      loss_weight: 1.0
      type: CrossEntropyLoss
      use_sigmoid: true
    loss_loc:
      alpha: 0.25
      gamma: 2.0
      loss_weight: 1.0
      type: FocalLoss
      use_sigmoid: true
    loss_shape:
      beta: 0.2
      loss_weight: 1.0
      type: BoundedIoULoss
    square_anchor_generator:
      ratios:
      - 1.0
      scales:
      - 8
      strides:
      - 4
      - 8
      - 16
      - 32
      - 64
      type: AnchorGenerator
    type: GARPNHead
  test_cfg:
    rcnn:
      max_per_img: 100
      nms:
        iou_threshold: 0.5
        type: nms
      score_thr: 0.001
    rpn:
      max_per_img: 300
      min_bbox_size: 0
      nms:
        iou_threshold: 0.7
        type: nms
      nms_post: 1000
      nms_pre: 1000
  train_cfg:
    rcnn:
      assigner:
        ignore_iof_thr: -1
        match_low_quality: false
        min_pos_iou: 0.6
        neg_iou_thr: 0.6
        pos_iou_thr: 0.6
        type: MaxIoUAssigner
      debug: false
      pos_weight: -1
      sampler:
        add_gt_as_proposals: true
        neg_pos_ub: -1
        num: 256
        pos_fraction: 0.25
        type: RandomSampler
    rpn:
      allowed_border: -1
      assigner:
        ignore_iof_thr: -1
        match_low_quality: true
        min_pos_iou: 0.3
        neg_iou_thr: 0.3
        pos_iou_thr: 0.7
        type: MaxIoUAssigner
      center_ratio: 0.2
      debug: false
      ga_assigner:
        ignore_iof_thr: -1
        min_pos_iou: 0.3
        neg_iou_thr: 0.3
        pos_iou_thr: 0.7
        type: ApproxMaxIoUAssigner
      ga_sampler:
        add_gt_as_proposals: false
        neg_pos_ub: -1
        num: 256
        pos_fraction: 0.5
        type: RandomSampler
      ignore_ratio: 0.5
      pos_weight: -1
      sampler:
        add_gt_as_proposals: false
        neg_pos_ub: -1
        num: 256
        pos_fraction: 0.5
        type: RandomSampler
    rpn_proposal:
      max_per_img: 300
      min_bbox_size: 0
      nms:
        iou_threshold: 0.7
        type: nms
      nms_post: 1000
      nms_pre: 2000
  type: FasterRCNN
