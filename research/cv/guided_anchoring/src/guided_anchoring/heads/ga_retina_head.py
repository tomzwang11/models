# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
#
# This file has been derived from the
# https://github.com/open-mmlab/mmdetection/tree/v2.28.2
# repository and modified.
# ============================================================================
from collections import OrderedDict

import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops

from ..ops import MaskedConv2d
from .guided_anchor_head import FeatureAdaption, GuidedAnchorHead


class GARetinaHead(GuidedAnchorHead):
    """Guided-Anchor-based RetinaNet head."""

    def __init__(self,
                 num_classes,
                 in_channels,
                 stacked_convs=4,
                 conv_cfg=None,
                 norm_cfg=None,
                 init_cfg=None,
                 **kwargs):
        if init_cfg is None:
            init_cfg = dict(
                type='Normal',
                layer='Conv2d',
                std=0.01,
                override=[
                    dict(
                        type='Normal',
                        name='conv_loc',
                        std=0.01,
                        bias_prob=0.01),
                    dict(
                        type='Normal',
                        name='retina_cls',
                        std=0.01,
                        bias_prob=0.01)
                ])
        self.stacked_convs = stacked_convs
        self.conv_cfg = conv_cfg
        self.norm_cfg = norm_cfg
        self.min_float_num = -65500.0
        super().__init__(num_classes, in_channels, **kwargs)

        # Init NMS ops
        self.nms_op = ops.NMSWithMask(self.test_cfg['nms']['iou_threshold'])

        self.max_num = -1

        self.score_thr = self.test_cfg['score_thr']
        self.max_per_img = self.test_cfg['max_per_img']

        self.sort = ops.Sort(axis=-1, descending=True)

    def _init_layers(self):
        """Initialize layers of the head."""
        self.relu = nn.ReLU()

        self.cls_convs = nn.CellList()
        self.reg_convs = nn.CellList()

        for i in range(self.stacked_convs):
            chn = self.in_channels if i == 0 else self.feat_channels
            cls_conv = nn.SequentialCell(
                OrderedDict(
                    [(
                        'conv',
                        nn.Conv2d(
                            chn,
                            self.feat_channels,
                            3,
                            stride=1,
                            padding=1,
                            pad_mode='pad',
                            has_bias=True
                        )
                    )]
                )
            )
            self.cls_convs.append(cls_conv)

            reg_conv = nn.SequentialCell(
                OrderedDict(
                    [(
                        'conv',
                        nn.Conv2d(
                            chn,
                            self.feat_channels,
                            3,
                            stride=1,
                            padding=1,
                            pad_mode='pad',
                            has_bias=True
                        )
                    )]
                )
            )
            self.reg_convs.append(reg_conv)

        self.conv_loc = nn.Conv2d(self.feat_channels, 1, 1,
                                  has_bias=True)
        self.conv_shape = nn.Conv2d(self.feat_channels, self.num_anchors * 2,
                                    1, has_bias=True)
        self.feature_adaption_cls = FeatureAdaption(
            self.feat_channels,
            self.feat_channels,
            kernel_size=3,
            deform_groups=self.deform_groups)
        self.feature_adaption_reg = FeatureAdaption(
            self.feat_channels,
            self.feat_channels,
            kernel_size=3,
            deform_groups=self.deform_groups)
        self.retina_cls = MaskedConv2d(
            self.feat_channels,
            self.num_base_priors * self.cls_out_channels,
            3,
            padding=1,
            pad_mode='pad'
        )
        self.retina_reg = MaskedConv2d(
            self.feat_channels,
            self.num_base_priors * 4,
            3,
            padding=1,
            pad_mode='pad')

    def forward_single(self, x):
        """Forward feature map of a single scale level."""
        cls_feat = x
        reg_feat = x

        for cls_conv in self.cls_convs:
            cls_feat = cls_conv(cls_feat)
            cls_feat = self.relu(cls_feat)
        for reg_conv in self.reg_convs:
            reg_feat = reg_conv(reg_feat)
            reg_feat = self.relu(reg_feat)

        loc_pred = self.conv_loc(cls_feat)
        shape_pred = self.conv_shape(reg_feat)

        cls_feat = self.feature_adaption_cls(cls_feat, shape_pred)
        reg_feat = self.feature_adaption_reg(reg_feat, shape_pred)

        if not self.training:
            mask = ops.sigmoid(loc_pred) >= self.loc_filter_thr
            mask = mask[:, 0, :, :]
        else:
            mask = None

        cls_score = self.retina_cls(cls_feat, mask)
        bbox_pred = self.retina_reg(reg_feat, mask)
        return cls_score, bbox_pred, shape_pred, loc_pred

    def simple_test(self, feats, img_metas, rescale=False):
        """
        Test function without test-time augmentation.

        Args:
        ----
            feats (tuple[Tensor]): Multi-level features from the
                upstream network, each is a 4D-tensor.
            img_metas: List of image information.
            rescale (bool, optional): Whether to rescale the results.
                Defaults to False.

        Returns:
        -------
            list[tuple[Tensor, Tensor]]: Each item in result_list is 2-tuple.
                The first item is ``bboxes`` with shape (n, 5),
                where 5 represent (tl_x, tl_y, br_x, br_y, score).
                The shape of the second tensor in the tuple is ``labels``
                with shape (n, ).
        """
        return self.simple_test_bboxes(feats, img_metas, rescale=rescale)

    def forward(self, feats):
        """
        Forward features from the upstream network.

        Args:
        ----
            feats (tuple[Tensor]): Features from the upstream network, each is
                a 4D-tensor.

        Returns:
        -------
            tuple: A tuple of classification scores and bbox prediction.

                - cls_scores (list[Tensor]): Classification scores for all \
                    scale levels, each is a 4D-tensor, the channels number \
                    is num_base_priors * num_classes.
                - bbox_preds (list[Tensor]): Box energies / deltas for all \
                    scale levels, each is a 4D-tensor, the channels number \
                    is num_base_priors * 4.
        """
        cls_scores = []
        bbox_preds = []
        shape_preds = []
        loc_preds = []
        for i in range(self.num_layers):
            cls_score, bbox_pred, shape_pred, loc_pred = self.forward_single(
                feats[i]
            )

            cls_scores.append(cls_score)
            bbox_preds.append(bbox_pred)
            shape_preds.append(shape_pred)
            loc_preds.append(loc_pred)
        return cls_scores, bbox_preds, shape_preds, loc_preds

    def forward_train(self,
                      x,
                      img_metas,
                      gt_bboxes,
                      gt_labels=None,
                      gt_bboxes_ignore=None,
                      proposal_cfg=None,
                      **kwargs):
        """
        Args:
        ----
            x (list[Tensor]): Features from FPN.
            img_metas: Meta information of each image, e.g.,
                image size, scaling factor, etc.
            gt_bboxes (Tensor): Ground truth bboxes of the image,
                shape (num_gts, 4).
            gt_labels (Tensor): Ground truth labels of each box,
                shape (num_gts,).
            gt_bboxes_ignore (Tensor): Ground truth bboxes to be
                ignored, shape (num_ignored_gts, 4).
            proposal_cfg (mmcv.Config): Test / postprocessing configuration,
                if None, test_cfg would be used.

        Returns:
        -------
            tuple:
                losses: (dict[str, Tensor]): A dictionary of loss components.
                proposal_list (list[Tensor]): Proposals of each image.
        """
        # outs is tuple (cls_scores, bbox_preds, shape_preds, loc_preds)
        # cls_scores[i] has shape [B, num_classes, H, W]
        # bbox_preds[i] has shape [B, 4, H, W]
        # shape_preds[i] has shape [B, 2, H, W]
        # loc_preds[i] hasa shape [B, 1, H, W]
        # where i is FPN level
        outs = self.forward(x)
        if gt_labels is None:
            loss_inputs = outs + (gt_bboxes, img_metas)
        else:
            loss_inputs = outs + (gt_bboxes, gt_labels, img_metas)
        losses = self.loss(*loss_inputs, gt_bboxes_ignore=gt_bboxes_ignore)
        if proposal_cfg is None:
            return losses
        proposal_list = self.get_bboxes(
            *outs, img_metas=img_metas, cfg=proposal_cfg)
        return losses, proposal_list

    def simple_test_bboxes(self, feats, img_metas, rescale=False):
        outs = self.forward(feats)

        results_list = self.get_bboxes(
            *outs, img_metas=img_metas, rescale=rescale)
        return results_list

    def get_bboxes(self,
                   cls_scores,
                   bbox_preds,
                   shape_preds,
                   loc_preds,
                   img_metas,
                   cfg=None,
                   rescale=False):

        featmap_sizes = [featmap.shape[-2:] for featmap in cls_scores]
        # get guided anchors
        _, guided_anchors, loc_masks = self.get_anchors(
            featmap_sizes,
            shape_preds,
            loc_preds,
            img_metas,
            use_loc_filter=not self.training)

        result_list = []
        for img_id in range(len(img_metas)):
            cls_score_list = [
                cls_scores[i][img_id] for i in range(self.num_layers)
            ]
            bbox_pred_list = [
                bbox_preds[i][img_id] for i in range(self.num_layers)
            ]
            guided_anchor_list = [
                guided_anchors[img_id][i] for i in range(self.num_layers)
            ]
            loc_mask_list = [
                loc_masks[img_id][i] for i in range(self.num_layers)
            ]
            img_shape = img_metas[img_id]

            proposals = self._get_bboxes_single(cls_score_list, bbox_pred_list,
                                                guided_anchor_list,
                                                loc_mask_list, img_shape,
                                                1.0, cfg, rescale)
            result_list.append(proposals)
        return result_list

    def _get_bboxes_single(self,
                           cls_scores,
                           bbox_preds,
                           mlvl_anchors,
                           mlvl_masks,
                           img_shape,
                           scale_factor=1.0,
                           cfg=None,
                           rescale=False):
        all_bboxes = []
        all_scores = []
        all_masks = []
        for cls_score, bbox_pred, anchors, mask in zip(cls_scores, bbox_preds,
                                                       mlvl_anchors,
                                                       mlvl_masks):
            assert cls_score.shape[-2:] == bbox_pred.shape[-2:]
            # reshape scores and bbox_pred
            cls_score = ops.transpose(
                cls_score, (1, 2, 0)
            ).reshape(-1, self.cls_out_channels)
            scores = ops.sigmoid(cls_score)
            bbox_pred = ops.transpose(
                bbox_pred, (1, 2, 0)
            ).reshape(-1, 4)
            mask_for_scores = ops.broadcast_to(
                ops.expand_dims(mask, 1),
                (mask.shape[0],
                 self.num_classes)
            )
            scores = ops.masked_fill(
                scores, ops.logical_not(mask_for_scores), self.min_float_num
            )

            if self.nms_pre > 0 and mask.shape[0] > self.nms_pre:
                max_scores = scores.max(axis=1)
                _, topk_inds = ops.top_k(max_scores, self.nms_pre,
                                         sorted=False)
                topk_inds = ops.expand_dims(topk_inds, 1)
                anchors = ops.gather_nd(anchors, topk_inds)
                bbox_pred = ops.gather_nd(bbox_pred, topk_inds)
                scores = ops.gather_nd(scores, topk_inds)
                mask = ops.gather_nd(mask, topk_inds)
            bboxes = self.bbox_coder.decode(
                anchors, bbox_pred, max_shape=img_shape)
            all_bboxes.append(bboxes)
            all_scores.append(scores)
            all_masks.append(mask)

        all_bboxes = ops.concat(all_bboxes)
        all_scores = ops.concat(all_scores)
        # all_masks = ops.concat(all_masks)
        # if rescale:
        #     mlvl_bboxes /= mlvl_bboxes.new_tensor(scale_factor)

        if self.use_sigmoid_cls:
            # Add a dummy background class to the backend when using sigmoid
            # remind that we set FG labels to [0, num_class-1] since mmdet v2.0
            # BG cat_id: num_class
            padding = ops.zeros((all_scores.shape[0], 1), all_scores.dtype)
            all_scores = ops.concat([all_scores, padding], axis=1)

        return all_bboxes, all_scores

    def loss_single(self, cls_score, bbox_pred, anchors, labels, label_weights,
                    bbox_targets, bbox_weights, num_total_samples):
        """
        Compute loss of a single scale level.

        Args:
        ----
            cls_score (Tensor): Box scores for each scale level
                Has shape (N, num_anchors * num_classes, H, W).
            bbox_pred (Tensor): Box energies / deltas for each scale
                level with shape (N, num_anchors * 4, H, W).
            anchors (Tensor): Box reference for each scale level with shape
                (N, num_total_anchors, 4).
            labels (Tensor): Labels of each anchors with shape
                (N, num_total_anchors).
            label_weights (Tensor): Label weights of each anchor with shape
                (N, num_total_anchors)
            bbox_targets (Tensor): BBox regression targets of each anchor
                weight shape (N, num_total_anchors, 4).
            bbox_weights (Tensor): BBox regression loss weights of each anchor
                with shape (N, num_total_anchors, 4).
            num_total_samples (int): If sampling, num total samples equal to
                the number of total anchors; Otherwise, it is the number of
                positive anchors.

        Returns:
        -------
            dict[str, Tensor]: A dictionary of loss components.
        """
        # classification loss
        labels = labels.flatten()
        label_weights = label_weights.flatten()
        label_weights = label_weights.astype(ms.float32)
        cls_score_t = ops.transpose(cls_score, (0, 2, 3, 1))
        cls_score = ops.reshape(cls_score_t, (-1, self.cls_out_channels))
        loss_cls = self.loss_cls(cls_score, labels, label_weights,
                                 avg_factor=num_total_samples)

        # regression loss
        bbox_targets = ops.Reshape()(bbox_targets, (-1, 4))
        bbox_weights = ops.Reshape()(bbox_weights, (-1, 4))
        bbox_pred = ops.Reshape()(
            ops.Transpose()(bbox_pred, (0, 2, 3, 1)),
            (-1, 4)
        )
        if self.reg_decoded_bbox:
            # When the regression loss (e.g. `IouLoss`, `GIouLoss`)
            # is applied directly on the decoded bounding boxes, it
            # decodes the already encoded coordinates to absolute format.
            anchors = ops.Reshape()(anchors, (-1, 4))
            bbox_pred = self.bbox_coder.decode(anchors, bbox_pred)
        loss_bbox = self.loss_bbox(bbox_pred, bbox_targets)
        loss_bbox = bbox_weights * loss_bbox
        loss_bbox = ops.reduce_sum(ops.reduce_mean(loss_bbox, -1), -1)
        return loss_cls, loss_bbox
