import operator
import collections
from collections import OrderedDict
from itertools import islice, repeat
import mindspore.nn as nn
import mindspore.ops as ops

def _ntuple(n):
    def parse(x):
        if isinstance(x, collections.abc.Iterable):
            return tuple(x)
        return tuple(repeat(x, n))
    return parse
_pair = _ntuple(2)

class TwoConv2d(nn.Cell):
    def __init__(self, in_channels, out_channels, kernel_size, \
                 stride=1, padding=0, dilation=1, groups=1, bias=True):
        kernel_size = _pair(kernel_size)
        stride = _pair(stride)
        padding = _pair(padding)
        dilation = _pair(dilation)
        super(TwoConv2d, self).__init__(
            in_channels, out_channels, kernel_size, stride, padding, dilation,
            False, _pair(0), groups, bias, padding_mode='zeros')

    def forward(self, inputs, idx=0):
        # return F.conv2d(input, self.weight, self.bias, self.stride, self.padding, self.dilation, self.groups), idx
        return ops.conv2d(inputs, self.weight, pad_mode='pad', \
                          padding=self.padding, stride=self.stride, dilation=self.dilation, group=self.groups), idx

class AugBatchNorm(nn.Cell):
    _version = 1

    def __init__(self, num_features, eps=1e-5, num_classes=2, \
                 momentum=0.5, sync=False, affine=True, track_running_stats=True):
        super(AugBatchNorm, self).__init__()
        if sync:
            self.bns = nn.CellList([nn.SyncBatchNorm(num_features, eps, momentum, affine) for _ in range(num_classes)])
        else:
            self.bns = nn.CellList([nn.BatchNorm2d(num_features, eps, momentum, affine) for _ in range(num_classes)])

    def reset_running_stats(self):
        for bn in self.bns:
            bn.reset_running_stats()

    def reset_parameters(self):
        for bn in self.bns:
            bn.reset_parameters()

    def _check_input_dim(self, inputs):
        if inputs.dim() != 4:
            raise ValueError('expected 4D input (got {}D input)'
                             .format(inputs.dim()))

    def construct(self, x, idx=0): #idx 0 --> weak bn; idx 1 --> strong bn
        self._check_input_dim(x)
        bn = self.bns[idx]
        return bn(x), idx

class TwoInputSequential(nn.Cell):
    r"""A sequential container forward with two inputs.
    """
    def __init__(self, *args):
        super(TwoInputSequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.insert_child_to_cell(key, module)
        else:
            for idx, module in enumerate(args):
                self.insert_child_to_cell(str(idx), module)

    def _get_item_by_idx(self, iterator, idx):
        """Get the idx-th item of the iterator"""
        size = len(self)
        idx = operator.index(idx)
        if not -size <= idx < size:
            raise IndexError('index {} is out of range'.format(idx))
        idx %= size
        return next(islice(iterator, idx, None))

    def __getitem__(self, idx):
        if isinstance(idx, slice):
            return TwoInputSequential(OrderedDict(list(self._modules.items())[idx]))
        return self._get_item_by_idx(self._modules.values(), idx)

    def __setitem__(self, idx, module):
        key = self._get_item_by_idx(self._modules.keys(), idx)
        return setattr(self, key, module)

    def __delitem__(self, idx):
        if isinstance(idx, slice):
            for key in list(self._modules.keys())[idx]:
                delattr(self, key)
        else:
            key = self._get_item_by_idx(self._modules.keys(), idx)
            delattr(self, key)

    def __len__(self):
        return len(self._cells)

    def __dir__(self):
        keys = super(TwoInputSequential, self).__dir__()
        keys = [key for key in keys if not key.isdigit()]
        return keys

    def construct(self, input1, input2):
        for module in self._cells.values():
            input1, input2 = module(input1, input2)
        return input1, input2
