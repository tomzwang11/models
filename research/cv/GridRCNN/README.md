# Contents

* [Contents](#contents)
    * [Grid R-CNN Description](#gridrcnn-description)
        * [Model Architecture](#model-architecture)
        * [Dataset](#dataset)
    * [Environment Requirements](#environment-requirements)
    * [Quick Start](#quick-start)
        * [Prepare the model](#prepare-the-model)
        * [Run the scripts](#run-the-scripts)
    * [Script Description](#script-description)
        * [Script and Sample Code](#script-and-sample-code)
        * [Script Parameters](#script-parameters)
    * [Training](#training)
        * [Training Process](#training-process)
        * [Transfer Training](#transfer-training)
        * [Distribute training](#distribute-training)
    * [Export](#export)
        * [Export process](#export-process)
        * [Export result](#export-result)
    * [Evaluation](#evaluation)
        * [Evaluation Process](#evaluation-process)
            * [Evaluation on GPU](#evaluation-on-gpu)
            * [ONNX Evaluation](#onnx-evaluation)
        * [Evaluation result](#evaluation-result)
    * [Inference](#inference)
        * [Inference Process](#inference-process)
            * [Inference on GPU](#inference-on-gpu)
            * [Inference with ONNX](#inference-with-onnx)
        * [Inference result](#inference-result)
   * [Model Description](#model-description)
        * [Performance](#performance)
   * [Description of Random Situation](#description-of-random-situation)
   * [ModelZoo Homepage](#modelzoo-homepage)

## [Grid R-CNN Description](#contents)

Grid R-CNN adopts a grid guided localization mechanism for accurate object
detection. Different from the traditional regression based methods, the
Grid R-CNN captures the spatial information explicitly and enjoys the position
sensitive property of fully convolutional architecture. Instead of using only
two independent points, the model has a multi-point supervision formulation to
encode more clues in order to reduce the impact of inaccurate prediction of
specific points. To take the full advantage of the correlation of points in a
grid, the model proposes a two-stage information fusion strategy to fuse
feature maps of neighbor grid points.

[Paper](https://arxiv.org/abs/1811.12030): Xin Lu, Buyu Li, Yuxin Yue,
Quanquan Li, Junjie Yan. Computer Vision and Pattern Recognition (CVPR), 2018 (In press).

### [Model Architecture](#contents)

Grid R-CNN proposes standare Faster R-CNN architecture with some differences.
**Overview of the pipeline of Grid R-CNN:** Region proposals are obtained from
RPN and used for RoI feature extraction from the output feature maps of a CNN
backbone. The RoI features are then used to perform classification and
localization. In contrast to previous detectors with a box offset regression
branch, the current model adopts a grid guided mechanism for high quality
localization. The grid prediction branch adopts a FCN to output a probability
heatmap from which we can locate the grid points in the bounding box aligned
with the object. With the grid points, the model finally determines the
accurate object bounding box by a feature map level information fusion
approach.

Grid R-CNN result pipeline:

1. Backbone (ResNet)
2. Neck (FPN)
3. RPN
4. Proposal generator.
5. first ROI extractor (based on ROIAlign operation)
6. BboxHeadGrid (standard RCNN without localization branch)
7. multiclass NMS (reduce number of proposed boxes and omit objects with low
 confidence)
8. second ROI extractor (based on ROIAlign operation)
9. GridHead block (instead regression branch, generate mask that is used to
 refine object localization)

### [Dataset](#contents)

Note that you can run the scripts based on the dataset mentioned in original
paper or widely used in relevant domain/network architecture. In the following
sections, we will introduce how to run the scripts using the related dataset
below.

Dataset used: [COCO-2017](https://cocodataset.org/#download)

* Dataset size: 25.4G
    * Train: 18.0G，118287 images
    * Val: 777.1M，5000 images
    * Test: 6.2G，40670 images
    * Annotations: 474.7M, represented in 3 JSON files for each subset.
* Data format: image and json files.
    * Note: Data will be processed in dataset.py

## [Environment Requirements](#contents)

* Install [MindSpore](https://www.mindspore.cn/install/en).
* Download the dataset COCO-2017.
* Install third-parties requirements:

```text
Cython~=0.28.5
mindinsight
numpy~=1.21.2
opencv-python~=4.5.4.58
pycocotools>=2.0.5
matplotlib
seaborn
pandas
onnxruntime-gpu
tqdm==4.64.1
```

* We use COCO-2017 as training dataset in this example by default, and you
 can also use your own datasets. Dataset structure:

```shell
.
└── coco-2017
    ├── train
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    ├── validation
    │   ├── data
    │   │    ├── 000000000001.jpg
    │   │    ├── 000000000002.jpg
    │   │    └── ...
    │   └── labels.json
    └── test
        ├── data
        │    ├── 000000000001.jpg
        │    ├── 000000000002.jpg
        │    └── ...
        └── labels.json
```

Also we make evaluation only on mindrecord converted dataset. Use the
`convert_dataset.py` script to convert original COCO subset to mindrecord.

```shell
python convert_dataset.py --config_path default_config.yaml --converted_coco_path data/coco-2017/validation --converted_mindrecord_path data/mindrecord/validation
```

Result mindrecord dataset will have next format:

```shell
.
└── mindrecord
    ├── validation
    │   ├── file.mindrecord
    │   ├── file.mindrecord.db
    │   └── labels.json
    └── test
        ├── file.mindrecord
        ├── file.mindrecord.db
        └── labels.json
```

It is possible to convert train dataset to mindrecord format and use it to
train model.

## [Quick Start](#contents)

### [Prepare the model](#contents)

1. Prepare yaml config file. Create file and copy content from
 `default_config.yaml` to created file.
1. Change data settings: experiment folder (`train_outputs`), image size
 settings (`img_width`, `img_height`, etc.), subsets folders (`train_dataset`,
 `val_dataset`, `train_data_type`, `val_data_type`), information about
 categories etc.
1. Change the backbone settings: type (`backbone`), path to pretrained ImageNet
 weights, layer freezing settings.
1. Change other training hyper parameters (learning rate, regularization,
 augmentations etc.).

### [Run the scripts](#contents)

After installing MindSpore via the official website, you can start training and
evaluation as follows:

* running on GPU

```shell
# distributed training on GPU
bash scripts/run_distribute_train_gpu.sh [CONFIG_PATH] [DEVICE_NUM] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]

# standalone training on GPU
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]

# run eval on GPU
bash scripts/run_eval_gpu.sh [CONFIG_PATH] [VAL_DATA] [CHECKPOINT_PATH] (Optional)[PREDICTION_PATH]
```

## [Script Description](#contents)

### [Script and Sample Code](#contents)

```shell
.
└─ cv
  └─ GridRcnn
    ├─ README.md                       ## descriptions about Grid R-CNN
    ├─ scripts
    |  ├── run_distribute_train_gpu.sh  ## bash script for distributed on gpu
    |  ├── run_eval_gpu.sh              ## bash script for eval on gpu
    |  ├── run_eval_onnx.sh             ## bash script for onnx model evaluation
    |  ├── run_infer_gpu.sh             ## bash script for pred on gpu
    |  ├── run_infer_onnx.sh            ## bash script for predictions using ONNX
    |  └── run_standalone_train_gpu.sh  ## bash script for distributed on gpu
    ├─ src
    |  ├── GridRcnn
    |  |    ├── __init__.py                  ## inti file
    |  |    ├── anchor_generator.py          ## anchor generator class
    |  |    ├── bbox_assign_sample.py        ## assigner and sampler class to train RPN
    |  |    ├── bbox_assign_sample_stage2.py ## assigner and sampler class to train RCNN
    |  |    ├── bbox_head_grid.py            ## RCNN class without localization branch
    |  |    ├── fpn_neck.py                  ## Feature Pyramid Network
    |  |    ├── grid_head.py                 ## Grid Head block for bbox localization
    |  |    ├── grid_rcnn.py                 ## Grid R-CNN main class
    |  |    ├── proporsal_generator.py       ## Proposals generator class
    |  |    ├── resnet.py                    ## ResNet backbone
    |  |    ├── resnext.py                   ## ResNext backbone
    |  |    ├── roi_align.py                 ## ROI extractor (based on ROIAlign operation)
    |  |    └── rpn.py                       ## Region Proposal Network
    |  ├── model_utils
    |  |    ├── __init__.py                  ## init file
    |  |    ├── config.py                    ## configuration file parsing utils
    |  |    ├── device_adapter.py            ## file to adapt current used devices
    |  |    ├── local_adapter.py             ## file to work with local devices
    |  |    └── moxing_adapter.py            ## file to work with model arts devices
    |  ├── __init__.py                   ## init file
    |  ├── callback.py                   ## callbacks
    |  ├── common.py                     ## common functional with common setting
    |  ├── dataset.py                    ## images loading and preprocessing
    |  ├── detecteval.py                 ## DetectEval class to analyze predictions
    |  ├── eval_utils.py                 ## evaluation metrics
    |  ├── lr_schedule.py                ## optimizer settings
    |  ├── mlflow_funcs.py               ## mlflow utilities
    |  └── network_define.py             ## model wrappers for training
    ├── __init__.py                        ## init file
    ├── draw_predictions.py                ## draw inference results on single image
    ├── convert_dataset.py                 ## script to convert dataset to mindrecord format
    ├── default_config.yaml                ## default configuration file
    ├── eval.py                            ## eval script (ckpt)
    ├── eval_onnx.py                       ## eval script (onnx)
    ├── export.py                          ## export mindir, onnx, air script
    ├── infer.py                           ## inference script (ckpt)
    ├── infer_onnx.py                      ## inference script (onnx)
    ├── requirements.txt                   ## list of requirements
    ├── resnet50_config.yaml               ## configuration file
    └── train.py                           ## train script
```

### [Script Parameters](#contents)

Major parameters in the yaml config file as follows:

```shell
train_outputs: '/data/grid_rcnn_models'                         ## Path to folder with experiments  
brief: 'gpu-1_1024x1024'                                        ## Experiment short name
device_target: GPU                                              ## Device
# ===================================================
# backbone
backbone_type: "resnet"                                         ## Backbone type
resnet_depth: 50                                                ## Backbone depth
backbone_pretrained: '/data/backbones/resnet50.ckpt'            ## Path to pretrained backbone
resnet_frozen_stages: 1                                         ## Number of frozen stages (use if backbone is pretrained, set -1 to not freeze)
resnet_norm_eval: True                                          ## Whether make batch normalization work in eval mode all time (use if backbone is pretrained)
resnet_num_stages: 4                                            ## Number of resnet/resnext stages
resnet_out_indices: [0, 1, 2, 3]                                ## Define indices of resnet/resnext outputs
resnext_groups: 1                                               ## Group number in ResNext layers
resnext_base_width: 4                                           ## Base width in ResNext layers

# fpn
fpn_in_channels: [256, 512, 1024, 2048]                         ## Channels number for each input feature map (FPN)
fpn_out_channels: 256                                           ## Channels number for each output feature map (FPN)
fpn_num_outs: 5                                                 ## Number of output feature map (FPN)

# rpn
rpn_in_channels: 256                                            ## Input channels
rpn_feat_channels: 256                                          ## Number of channels in intermediate feature map in RPN
rpn_loss_cls_weight: 1.0                                        ## RPN classification loss weight
rpn_loss_reg_weight: 1.0                                        ## RPN localization loss weight
rpn_cls_out_channels: 1                                         ## Output classes number
rpn_target_means: [0., 0., 0., 0.]                              ## Parameter for bbox encoding (RPN targets generation)
rpn_target_stds: [1.0, 1.0, 1.0, 1.0]                           ## Parameter for bbox encoding (RPN targets generation)
smoothl1_beta: 0.1111111111111                                  ## Parameter for localization loss

# anchor
anchor_scales: [8]                                              ## Anchor scales
anchor_ratios: [0.5, 1.0, 2.0]                                  ## Anchor ratios
anchor_strides: [4, 8, 16, 32, 64]                              ## Anchor strides for each fetature map
num_anchors: 3                                                  ## Parameter must be equal to number of ratios

# bbox_assign_sampler
neg_iou_thr: 0.3                                                ##
pos_iou_thr: 0.7                                                ##
min_pos_iou: 0.3                                                ##
num_gts: 128                                                    ## Max number of bboxes in gt tensor and predictions
num_expected_neg: 256                                           ## Max number of negative samples in RPN
num_expected_pos: 128                                           ## Max nuvber of positive samples in RPN

# proposal
activate_num_classes: 2                                         ##
use_sigmoid_cls: True                                           ## Whether use sigmoid or softmax to obtaine confidence

# bbox_assign_sampler_stage2
neg_iou_thr_stage2: 0.5                                         ## IoU thresholds
pos_iou_thr_stage2: 0.5                                         ##
min_pos_iou_stage2: 0.5                                         ##
num_bboxes_stage2: 2000                                         ##
num_expected_pos_stage2: 128                                    ## Max number of negative samples in RCNN
num_expected_neg_stage2: 512                                    ## Max nuvber of positive samples in RCNN

# roi_align
roi_layer: {type: 'RoIAlign', out_size: 7, sample_num: 2}       ## RoI configuration
roi_align_out_channels: 256                                     ##
roi_align_featmap_strides: [4, 8, 16, 32]                       ##
roi_align_finest_scale: 56                                      ##
roi_sample_num: 640                                             ##

# rcnn
rcnn_num_fcs: 2                                                 ## Number of layer before classification
rcnn_in_channels: 256                                           ## Number of input channels
rcnn_fc_out_channels: 1024                                      ## Number of intermediate channels before classfication
rcnn_loss_cls_weight: 1                                         ## Classification loss weight

# grid roi
grid_roi_layer: {type: 'RoIAlign', out_size: 14, sample_num: 2} ## RoI configuration for Grid Head block
grid_roi_align_out_channels: 256                                ##
grid_roi_align_featmap_strides: [4, 8, 16, 32]                  ##
grid_roi_align_finest_scale: 56                                 ##
grid_roi_sample_num: 128                                        ##

# grid
grid_num_convs: 8                                               ## Number of convolution layer before grid computation
grid_in_channels: 256                                           ## Number of input channels
grid_conv_out_channels: 576                                     ## Number of output channels of convolution layers
num_grids: 9                                                    ## Number of grid points
grid_mask_size: 56                                              ## Size of mask with drawn grids points
grid_radius: 1                                                  ## Radius of grid point
grid_loss_weight: 15                                            ## Grid loss weight

# train proposal
rpn_proposal_nms_across_levels: False                           ## Proposals generation settings during traing
rpn_proposal_nms_pre: 2000                                      ##
rpn_proposal_nms_post: 2000                                     ##
rpn_proposal_max_num: 2000                                      ##
rpn_proposal_nms_thr: 0.7                                       ##
rpn_proposal_min_bbox_size: 0                                   ##

# test proposal
rpn_nms_across_levels: False                                    ## Proposals generation settings during inference
rpn_nms_pre: 1000                                               ##
rpn_nms_post: 1000                                              ##
rpn_max_num: 1000                                               ##
rpn_nms_thr: 0.7                                                ##
rpn_min_bbox_size: 0                                            ##  
test_score_thr: 0.03                                            ##
test_iou_thr: 0.3                                               ##
test_batch_size: 1                                              ##

# optimizer
base_lr: 0.01                                                   ## Base learning rate
warmup_step: 3665                                               ## Number of warmup steps
warmup_ratio: 0.0125                                            ## Ininial learning rate = base_lr * warmup_ratio
milestones: [17, 23]                                            ## Epochs numbers when learning rate is divided by 10 (for multistep lr_type)
sgd_momentum: 0.9                                               ## Optimizer parameter
lr_type: "dynamic"                                              ## Learning rate changinb type
opt_type: "sgd"                                                 ## Optimizer type (sgd or adam)
grad_clip: 1.0                                                  ## Gradient clippint (set 0 to turn off)
weight_decay: 0.0001                                            ## Regularization

# train
batch_size: 4                                                   ## Train batch size
loss_scale: 256                                                 ## Loss scale
epoch_size: 25                                                  ## Number of epochs
run_eval: True                                                  ## Whether evaluation or not
eval_every: 1                                                   ## Evaluation interval
enable_graph_kernel: False                                      ## Turn on kernekl fusion
finetune: False                                                 ## Turn on finetune (for transfer learning)
datasink: False                                                 ## Turn on datasink mode
pre_trained: 0                                                  ## Path to pretrained model weights

#distribution training
run_distribute: False                                           ## Turn on distributed training
device_id: 0                                                    ##
device_num: 1                                                   ## Number of devices (only if distributed training turned on)
rank_id: 0                                                      ##

num_parallel_workers: 6                                         ## Number of threads used to process the dataset in parallel
python_multiprocessing: False                                   ## Parallelize Python operations with multiple worker processes

# dataset setting
train_data_type: 'coco'                                                         ## Train dataset type
val_data_type: 'mindrecord'                                                     ## Validation dataset type
train_dataset: '/data/coco-2017/train'                                          ## Path to train dataset
val_dataset: '/data/mindrecord_eval'                                            ## Path to validation dataset
coco_classes: ['background', 'person', 'bicycle', 'car', 'motorcycle',
               'airplane', 'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']
num_classes: 81                                                                 ## Number of classes (including background)
train_dataset_num: 0                                                         ## Parameter to reduce training data (debugging)
train_dataset_divider: 0                                                     ## Parameter to reduce training data (debugging)

# images
img_width: 1024                                                         ## Input images width
img_height: 1024                                                        ## Input images height
divider: 64                                                             ## Automatically make width and height are dividable by divider
img_mean: [123.675, 116.28, 103.53]                                     ## Image normalization parameters
img_std: [58.395, 57.12, 57.375]                                        ## Image normalization parameters
to_rgb: True                                                            ## RGB or BGR
keep_ratio: True                                                        ## Keep ratio in original images

# augmentation
flip_ratio: 0.5                                                         ## Probability of image horizontal flip
expand_ratio: 0.0                                                       ## Probability of image expansion

# callbacks
save_every: 100                                                         ## Save model everi <n> steps
keep_checkpoint_max: 5                                                  ## Max number of saved periodical checkpoints
keep_best_checkpoints_max: 5                                            ## Max number of saved best checkpoints
 ```

## [Training](#contents)

To train the model, run `train.py`.

### [Training process](#contents)

Standalone training mode:

```bash
bash scripts/run_standalone_train_gpu.sh [CONFIG_PATH] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]
```

We need several parameters for these scripts.

* `CONFIG_PATH`: path to config file.
* `TRAIN_DATA`: path to train dataset.
* `VAL_DATA`: path to validation dataset.
* `TRAIN_OUT`: path to folder with training experiments.
* `BRIEF`: short experiment name.
* `PRETRAINED_PATH`: the path of pretrained checkpoint file, it is better
 to use absolute path.

Training result will be stored in the current path, whose folder name is "LOG".
Under this, you can find checkpoint files together with result like the
following in log.

```log
[2023-03-07T20:09:42.156] INFO: Creating network...
[2023-03-07T20:09:42.794] INFO: Load backbone weights...
[2023-03-07T20:09:45.111] INFO: Number of parameters: 64601321
[2023-03-07T20:09:45.112] INFO: Device type: Others
[2023-03-07T20:09:45.112] INFO: Creating criterion, lr and opt objects...
[2023-03-07T20:09:45.656] INFO:     Done!

[2023-03-07T20:09:46.186] INFO: Directory created: /data/grid_rcnn_models/230307_GridRCNN_gpu-1_1024x1024/best_ckpt
[2023-03-07T20:16:22.914] INFO: Creating network...
[2023-03-07T20:16:23.518] INFO: Load backbone weights...
[2023-03-07T20:16:25.891] INFO: Number of parameters: 64601321
[2023-03-07T20:16:25.892] INFO: Device type: Others
[2023-03-07T20:16:25.892] INFO: Creating criterion, lr and opt objects...
[2023-03-07T20:16:26.430] INFO:     Done!

[2023-03-07T20:16:27.205] WARNING: Directory already exists: /data/grid_rcnn_models/230307_GridRCNN_gpu-1_1024x1024/best_ckpt
[2023-03-07T20:20:39.494] INFO: epoch: 1, step: 100, loss: 8.255971,  lr: 0.000394
[2023-03-07T20:23:07.467] INFO: epoch: 1, step: 200, loss: 6.503890,  lr: 0.000664
[2023-03-07T20:25:36.439] INFO: epoch: 1, step: 300, loss: 4.633539,  lr: 0.000933
[2023-03-07T20:28:05.456] INFO: epoch: 1, step: 400, loss: 3.990650,  lr: 0.001203
[2023-03-07T20:30:34.321] INFO: epoch: 1, step: 500, loss: 3.665144,  lr: 0.001472
[2023-03-07T20:33:03.232] INFO: epoch: 1, step: 600, loss: 2.978226,  lr: 0.001742
[2023-03-07T20:35:32.280] INFO: epoch: 1, step: 700, loss: 2.284767,  lr: 0.002011
[2023-03-07T20:38:01.521] INFO: epoch: 1, step: 800, loss: 1.895220,  lr: 0.002281
[2023-03-07T20:40:30.889] INFO: epoch: 1, step: 900, loss: 1.527057,  lr: 0.002550
[2023-03-07T20:43:00.279] INFO: epoch: 1, step: 1000, loss: 1.337446,  lr: 0.002819
[2023-03-07T20:45:29.622] INFO: epoch: 1, step: 1100, loss: 1.416805,  lr: 0.003089
[2023-03-07T20:47:58.995] INFO: epoch: 1, step: 1200, loss: 1.419195,  lr: 0.003358
[2023-03-07T20:50:28.208] INFO: epoch: 1, step: 1300, loss: 1.352841,  lr: 0.003628
[2023-03-07T20:52:57.526] INFO: epoch: 1, step: 1400, loss: 1.412287,  lr: 0.003897
[2023-03-07T20:55:26.719] INFO: epoch: 1, step: 1500, loss: 1.276485,  lr: 0.004167
[2023-03-07T20:57:55.750] INFO: epoch: 1, step: 1600, loss: 1.246478,  lr: 0.004436
[2023-03-07T21:00:24.732] INFO: epoch: 1, step: 1700, loss: 1.211365,  lr: 0.004705
[2023-03-07T21:02:53.909] INFO: epoch: 1, step: 1800, loss: 1.271623,  lr: 0.004975
[2023-03-07T21:05:22.842] INFO: epoch: 1, step: 1900, loss: 1.202264,  lr: 0.005244
[2023-03-07T21:07:51.788] INFO: epoch: 1, step: 2000, loss: 1.077011,  lr: 0.005514
[2023-03-07T21:10:20.781] INFO: epoch: 1, step: 2100, loss: 1.172696,  lr: 0.005783
[2023-03-07T21:12:49.723] INFO: epoch: 1, step: 2200, loss: 1.209667,  lr: 0.006053
[2023-03-07T21:15:18.752] INFO: epoch: 1, step: 2300, loss: 1.141532,  lr: 0.006322
[2023-03-07T21:17:47.616] INFO: epoch: 1, step: 2400, loss: 1.149880,  lr: 0.006592
[2023-03-07T21:45:35.656] INFO: Eval epoch time: 1604159.755 ms, per step time: 320.832 ms
[2023-03-07T21:46:05.402] INFO: Result metrics for epoch 1: {'loss': 2.2565435121965076, 'mAP': 0.0008585248850328383}
[2023-03-07T21:46:05.416] INFO: Train epoch time: 5377996.533 ms, per step time: 2201.390 ms
...
[2023-03-09T07:29:29.796] INFO: epoch: 25, step: 100, loss: 0.478965,  lr: 0.000430
[2023-03-09T07:31:59.616] INFO: epoch: 25, step: 200, loss: 0.480737,  lr: 0.000420
[2023-03-09T07:34:29.390] INFO: epoch: 25, step: 300, loss: 0.495364,  lr: 0.000411
[2023-03-09T07:36:59.219] INFO: epoch: 25, step: 400, loss: 0.474267,  lr: 0.000401
[2023-03-09T07:39:29.087] INFO: epoch: 25, step: 500, loss: 0.538386,  lr: 0.000391
[2023-03-09T07:41:58.940] INFO: epoch: 25, step: 600, loss: 0.512586,  lr: 0.000382
[2023-03-09T07:44:28.807] INFO: epoch: 25, step: 700, loss: 0.451431,  lr: 0.000372
[2023-03-09T07:46:58.598] INFO: epoch: 25, step: 800, loss: 0.508664,  lr: 0.000363
[2023-03-09T07:49:28.410] INFO: epoch: 25, step: 900, loss: 0.579792,  lr: 0.000354
[2023-03-09T07:51:58.262] INFO: epoch: 25, step: 1000, loss: 0.512156,  lr: 0.000345
[2023-03-09T07:54:28.129] INFO: epoch: 25, step: 1100, loss: 0.526274,  lr: 0.000336
[2023-03-09T07:56:57.806] INFO: epoch: 25, step: 1200, loss: 0.508825,  lr: 0.000327
[2023-03-09T07:59:27.654] INFO: epoch: 25, step: 1300, loss: 0.496414,  lr: 0.000318
[2023-03-09T08:01:57.426] INFO: epoch: 25, step: 1400, loss: 0.449683,  lr: 0.000309
[2023-03-09T08:04:27.275] INFO: epoch: 25, step: 1500, loss: 0.515948,  lr: 0.000301
[2023-03-09T08:06:56.963] INFO: epoch: 25, step: 1600, loss: 0.491729,  lr: 0.000293
[2023-03-09T08:09:26.609] INFO: epoch: 25, step: 1700, loss: 0.488687,  lr: 0.000284
[2023-03-09T08:11:56.380] INFO: epoch: 25, step: 1800, loss: 0.520289,  lr: 0.000276
[2023-03-09T08:14:26.227] INFO: epoch: 25, step: 1900, loss: 0.476070,  lr: 0.000268
[2023-03-09T08:16:55.968] INFO: epoch: 25, step: 2000, loss: 0.479716,  lr: 0.000260
[2023-03-09T08:19:25.639] INFO: epoch: 25, step: 2100, loss: 0.532733,  lr: 0.000252
[2023-03-09T08:21:55.391] INFO: epoch: 25, step: 2200, loss: 0.477974,  lr: 0.000245
[2023-03-09T08:24:25.079] INFO: epoch: 25, step: 2300, loss: 0.521292,  lr: 0.000237
[2023-03-09T08:26:54.921] INFO: epoch: 25, step: 2400, loss: 0.499607,  lr: 0.000230
[2023-03-09T08:54:10.953] INFO: Eval epoch time: 1571176.333 ms, per step time: 314.235 ms
[2023-03-09T08:54:43.093] INFO: Result metrics for epoch 25: {'loss': 0.5000397314104422, 'mAP': 0.1462293610846727}
[2023-03-09T08:54:43.106] INFO: Train epoch time: 5262133.511 ms, per step time: 2153.964 ms
```

### [Transfer Training](#contents)

You can train your own model based on either pretrained classification model
or pretrained detection model. You can perform transfer training by following
steps.

1. Prepare your dataset.
1. Change configuraino YAML file according to your own dataset, especially the
 change `num_classes` value and `coco_classes` list.
1. Prepare a pretrained checkpoint. You can load the pretrained checkpoint by
 `pretrained` argument. Transfer training means a new training job, so just set
 `finetune` True.
1. Run training.

### [Distribute training](#contents)

Distribute training mode:

```shell
bash scripts/run_distribute_train_gpu.sh [CONFIG_PATH] [DEVICE_NUM] [TRAIN_DATA] [VAL_DATA] [TRAIN_OUT] [BRIEF] (OPTIONAL)[PRETRAINED_PATH]
```

We need several parameters for this scripts:

* `CONFIG_PATH`: path to config file.
* `DEVICE_NUM`: number of devices.
* `TRAIN_DATA`: path to train dataset.
* `VAL_DATA`: path to validation dataset.
* `TRAIN_OUT`: path to folder with training experiments.
* `BRIEF`: short experiment name.
* `PRETRAINED_PATH`: the path of pretrained checkpoint file, it is better
 to use absolute path.

## [Export](#contents)

### [Export process](#contents)

* Export model.

```shell
python export.py --config_path [CONFIG_PATH] --export_ckpt_file [PATH_TO_CHECKPOINT] --export_file_format [FILE_FORMAT] --export_file_path [PATH_TO_OUTPUT]
```

`FILE_FORMAT` should be in ["AIR", "MINDIR", "ONNX"] (Currently export to air
format is not checked)

### [Export result](#contents)

Exporting result is file in ONNX or MINDIR format.

## [Evaluation](#contents)

### [Evaluation process](#contents)

#### [Evaluation on GPU](#contents)

```shell
bash scripts/run_eval_gpu.sh [CONFIG_PATH] [VAL_DATA] [CHECKPOINT_PATH] (Optional)[PREDICTION_PATH]
```

We need four parameters for this scripts.

* `CONFIG_PATH`: path to config file.
* `VAL_DATA`: the absolute path for dataset subset (validation).
* `CHECKPOINT_PATH`: path to checkpoint.
* `PREDICTION_PATH`: path to file with predictions JSON file (predictions may
 be saved to this file and loaded after).

> checkpoint can be produced in training process.

#### [ONNX Evaluation](#contents)

Run ONNX evaluation from grid_rcnn directory:

 ```bash
bash scripts/run_eval_onnx.sh [CONFIG_PATH] [VAL_DATA] [ONNX_PATH] [DEVICE_TARGET] (Optional)[PREDICTION_PATH]
```

We need three parameters for this scripts.

* `CONFIG_PATH`: path to config file.
* `VAL_DATA`: the absolute path for dataset subset (validation).
* `ONNX_PATH`： path to saved onnx model.
* `DEVICE_TARGET`: computation platform (GPU or CPU).
* `PREDICTION_PATH`: path to file with predictions JSON file (predictions may
 be saved to this file and loaded after).

### [Evaluation result](#contents)

Result for GPU:

```log
Loading and preparing results...
Converting ndarray to lists...
(201915, 7)
0/201915
DONE (t=1.16s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=32.64s).
Accumulating evaluation results...
DONE (t=6.78s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.401
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.580
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.428
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.223
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.440
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.532
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.335
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.542
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.569
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.358
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.608
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.731
Eval result: 0.40069177274275236
```

Result for ONNX:

```log
Loading and preparing results...
Converting ndarray to lists...
(201904, 7)
0/201904
DONE (t=1.24s)
creating index...
index created!
Running per image evaluation...
Evaluate annotation type *bbox*
DONE (t=30.49s).
Accumulating evaluation results...
DONE (t=5.40s).
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.401
 Average Precision  (AP) @[ IoU=0.50      | area=   all | maxDets=100 ] = 0.580
 Average Precision  (AP) @[ IoU=0.75      | area=   all | maxDets=100 ] = 0.428
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.223
 Average Precision  (AP) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.440
 Average Precision  (AP) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.532
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=  1 ] = 0.335
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets= 10 ] = 0.542
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=   all | maxDets=100 ] = 0.569
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= small | maxDets=100 ] = 0.358
 Average Recall     (AR) @[ IoU=0.50:0.95 | area=medium | maxDets=100 ] = 0.608
 Average Recall     (AR) @[ IoU=0.50:0.95 | area= large | maxDets=100 ] = 0.731
Eval result: 0.40069781358530604
```

## [Inference](#contents)

### [Inference process](#contents)

#### [Inference on GPU](#contents)

Run model inference from grid_rcnn directory:

```bash
bash scripts/run_infer_gpu.sh [CONFIG_PATH] [CHECKPOINT_PATH] [PRED_INPUT] [PRED_OUTPUT]
```

We need 4 parameters for these scripts:

* `CONFIG_FILE`： path to config file.
* `CHECKPOINT_PATH`: path to saved checkpoint.
* `PRED_INPUT`: path to input folder or image.
* `PRED_OUTPUT`: path to output JSON file.

#### [Inference with ONNX](#contents)

Run model inference from grid_rcnn directory:

```bash
bash scripts/run_infer_onnx.sh [CONFIG_PATH] [ONNX_PATH] [PRED_INPUT] [PRED_OUTPUT] [DEVICE_TARGET]
```

We need 4 parameters for this scripts:

* `CONFIG_FILE`： path to config file.
* `CHECKPOINT_PATH`: path to saved checkpoint.
* `PRED_INPUT`: path to input folder or image.
* `PRED_OUTPUT`: path to output JSON file.
* `DEVICE_TARGET`: used device (CPU or GPU)

### [Inference result](#contents)

Predictions will be saved in JSON file. Predictions format is same for
mindspore and ONNX model. File content is list of predictions for each image.
It's supported predictions for folder of images (png, jpeg file in folder root)
and single image.

Typical outputs of such script for single image:

```log
{
 "/data/coco-2017/validation/data/000000440617.jpg": {
  "height": 480,
  "width": 640,
  "predictions": [
   {
    "bbox": {
     "x_min": 69.29314422607422,
     "y_min": 188.06005859375,
     "width": 342.35968017578125,
     "height": 140.87567138671875
    },
    "class": {
     "label": 6,
     "category_id": "unknown",
     "name": "train"
    },
    "score": 0.9988755583763123
   },
   ...
  ]
 }
}
```

Typical outputs for folder with images:

```log
{
 "/data/coco-2017/validation/data/000000194832.jpg": {
  "height": 425,
  "width": 640,
  "predictions": [
   {
    "bbox": {
     "x_min": 282.27490234375,
     "y_min": 92.1941146850586,
     "width": 59.398284912109375,
     "height": 37.139488220214844
    },
    "class": {
     "label": 62,
     "category_id": "unknown",
     "name": "tv"
    },
    "score": 0.9287368059158325
   },
   {
    "bbox": {
     "x_min": 425.9646301269531,
     "y_min": 204.679443359375,
     "width": 214.41036987304688,
     "height": 220.695556640625
    },
    "class": {
     "label": 56,
     "category_id": "unknown",
     "name": "chair"
    },
    "score": 0.8844134211540222
   },
   ...
  ]
 },
 "/data/coco-2017/validation/data/000000104572.jpg": {
  "height": 419,
  "width": 640,
  "predictions": [
   {
    "bbox": {
     "x_min": 90.41325378417969,
     "y_min": 290.58258056640625,
     "width": 144.9475555419922,
     "height": 77.89364624023438
    },
    "class": {
     "label": 71,
     "category_id": "unknown",
     "name": "sink"
    },
    "score": 0.9929874539375305
   },
   ...
  ]
 },
 "/data/coco-2017/validation/data/000000463647.jpg": {
  "height": 480,
  "width": 640,
  "predictions": [
        ...
    ]
  }
  ...
}
```

## [Model Description](#contents)

### [Performance](#contents)

None

## [Description of Random Situation](#contents)

In dataset.py, we set the seed inside "create_dataset" function. We also use
random seed in train.py.

## [ModelZoo Homepage](#contents)

Please check the official [homepage](https://gitee.com/mindspore/models).
