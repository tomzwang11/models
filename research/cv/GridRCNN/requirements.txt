Cython~=0.28.5
mindinsight
mindspore-gpu==1.9.0
numpy~=1.21.2
opencv-python~=4.5.4.58
pycocotools>=2.0.5
matplotlib
seaborn
pandas
onnxruntime-gpu
tqdm==4.64.1
