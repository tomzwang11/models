# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================


# !/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import logging
import shutil
import yaml

import msadapter.pytorch as torch


def set_logging(name=None):
    rank = int(os.getenv('RANK', '-1'))
    logging.basicConfig(format="%(message)s", level=logging.INFO if (rank in (-1, 0)) else logging.WARNING)
    return logging.getLogger(name)


LOGGER = set_logging(__name__)
NCOLS = min(100, shutil.get_terminal_size().columns)


def load_yaml(file_path):
    """Load data from yaml file."""
    if isinstance(file_path, str):
        with open(file_path, errors='ignore') as f:
            data_dict = yaml.safe_load(f)
    return data_dict


def save_yaml(data_dict, save_path):
    """Save data to yaml file"""
    with open(save_path, 'w') as f:
        yaml.safe_dump(data_dict, f, sort_keys=False)


def write_tblog(tblogger, epoch, results, losses, cfg):
    """Display mAP and loss information to log."""
    tblogger.add_scalar("val/mAP@0.5", results[0], epoch + 1)
    tblogger.add_scalar("val/mAP@0.50:0.95", results[1], epoch + 1)

    tblogger.add_scalar("train/iou_loss", losses[0], epoch + 1)
    tblogger.add_scalar("train/dist_focalloss", losses[1], epoch + 1)
    tblogger.add_scalar("train/cls_loss", losses[2], epoch + 1)

    if 'loss' in cfg.keys():
        if 'FeatureAlignmentLoss' in cfg.loss:
            tblogger.add_scalar("train/feat_align_loss", losses[3], epoch + 1)

    tblogger.add_scalar("x/lr0", results[2], epoch + 1)
    tblogger.add_scalar("x/lr1", results[3], epoch + 1)
    tblogger.add_scalar("x/lr2", results[4], epoch + 1)


def write_tblog_full(tblogger, epoch, evaluate_results_full):
    tblogger.add_scalar("AP/AP-all    @ 0.50:0.95", evaluate_results_full[0], epoch + 1)
    tblogger.add_scalar('AP/AP-all    @ 0.50     ', evaluate_results_full[1], epoch + 1)
    tblogger.add_scalar('AP/AP-all    @ 0.75     ', evaluate_results_full[2], epoch + 1)
    tblogger.add_scalar('AP/AP-small  @ 0.50:0.95', evaluate_results_full[3], epoch + 1)
    tblogger.add_scalar('AP/AP-medium @ 0.50:0.95', evaluate_results_full[4], epoch + 1)
    tblogger.add_scalar('AP/AP-large  @ 0.50:0.95', evaluate_results_full[5], epoch + 1)
    tblogger.add_scalar('AR/AR-all-1      @ 0.50:0.95', evaluate_results_full[6], epoch + 1)
    tblogger.add_scalar('AR/AR-all-10     @ 0.50:0.95', evaluate_results_full[7], epoch + 1)
    tblogger.add_scalar('AR/AR-all-100    @ 0.50:0.95', evaluate_results_full[8], epoch + 1)
    tblogger.add_scalar('AR/AR-small-100  @ 0.50:0.95', evaluate_results_full[9], epoch + 1)
    tblogger.add_scalar('AR/AR-medium-100 @ 0.50:0.95', evaluate_results_full[10], epoch + 1)
    tblogger.add_scalar('AR/AR-large-100  @ 0.50:0.95', evaluate_results_full[11], epoch + 1)


def dicts_to_dict(dicts, result_dict=None, head=''):
    for key in dicts:
        value = dicts[key]
        if isinstance(value, dict):
            _result_dict = dicts_to_dict(value, result_dict, head=f'{head}{key}-')
            result_dict.update(_result_dict)
        elif not isinstance(value, (int, float, str, bool, torch.Tensor)):
            result_dict[f'{head}{key}'] = str(value)
        else:
            result_dict[f'{head}{key}'] = value
    return result_dict


def write_tbhparam(tblogger, evaluate_results_full, cfg, epoch):
    cfg_dict = dict(cfg._cfg_dict)
    hparam_dict = dicts_to_dict(cfg_dict)

    metric_dict = {
        'epoch': epoch,
        'AP/AP-all    @ 0.50:0.95': evaluate_results_full[0],
        'AP/AP-all    @ 0.50     ': evaluate_results_full[1],
        'AP/AP-all    @ 0.75     ': evaluate_results_full[2],
        'AP/AP-small  @ 0.50:0.95': evaluate_results_full[3],
        'AP/AP-medium @ 0.50:0.95': evaluate_results_full[4],
        'AP/AP-large  @ 0.50:0.95': evaluate_results_full[5],
        'AR/AR-all-1      @ 0.50:0.95': evaluate_results_full[6],
        'AR/AR-all-10     @ 0.50:0.95': evaluate_results_full[7],
        'AR/AR-all-100    @ 0.50:0.95': evaluate_results_full[8],
        'AR/AR-small-100  @ 0.50:0.95': evaluate_results_full[9],
        'AR/AR-medium-100 @ 0.50:0.95': evaluate_results_full[10],
        'AR/AR-large-100  @ 0.50:0.95': evaluate_results_full[11],
    }

    tblogger.add_hparams(hparam_dict, metric_dict)


def write_tbimg(tblogger, imgs, step, _type='train'):
    """Display train_batch and validation predictions to tensorboard."""
    if _type == 'train':
        tblogger.add_image(f'train_batch', imgs, step + 1, dataformats='HWC')
    elif _type == 'val':
        for idx, img in enumerate(imgs):
            tblogger.add_image(f'val_img_{idx + 1}', img, step + 1, dataformats='HWC')
    else:
        LOGGER.warning('WARNING: Unknown image type to visualize.\n')
