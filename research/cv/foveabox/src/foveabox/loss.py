# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import mindspore as ms
import mindspore.common.dtype as mstype
from mindspore import Tensor, nn
from mindspore.ops import functional as F
from mindspore.ops import operations as P


class SigmoidFocalClassificationLoss(nn.Cell):
    """
    Sigmoid focal-loss for classification.

    Args:
    ----
        gamma (float): Hyper-parameter to balance the easy and hard examples.
         Default: 2.0
        alpha (float): Hyper-parameter to balance the positive and negative
         example. Default: 0.25

    Returns:
    -------
        Tensor, the focal loss.
    """

    def __init__(self, gamma=2.0, alpha=0.25):
        super().__init__()
        self.ce_loss = nn.BCELoss(reduction='none')
        self.sigmoid = P.Sigmoid()
        self.pow = P.Pow()
        self.onehot = P.OneHot()
        self.on_value = Tensor(1.0, mstype.float32)
        self.off_value = Tensor(0.0, mstype.float32)
        self.gamma = gamma
        self.alpha = alpha
        self.eps = 1e-10

    def construct(self, logits, label, weight=None, avg_factor=None):
        label = self.onehot(label.astype(ms.int64), F.shape(logits)[-1],
                            self.on_value, self.off_value)
        sigmoid = self.sigmoid(logits)
        sigmoid_cross_entropy = self.ce_loss(sigmoid, label)
        label = F.cast(label, mstype.float32)
        p_t = label * sigmoid + (1 - label) * (1 - sigmoid)
        modulating_factor = self.pow(1 - p_t, self.gamma)
        alpha_weight_factor = (label * self.alpha +
                               (1 - label) * (1 - self.alpha))
        focal_loss = (modulating_factor * alpha_weight_factor *
                      sigmoid_cross_entropy)

        if weight is not None:
            if weight.shape != focal_loss.shape:
                if weight.shape[0] == focal_loss.shape[0]:
                    weight = weight.view((-1, 1))
            focal_loss = focal_loss * weight
        if avg_factor is not None:
            focal_loss = focal_loss.sum() / (avg_factor + self.eps)
        else:
            focal_loss = focal_loss.mean()

        return focal_loss
