# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

bottom_set = {
    'expert_hidden_units': [64],
    'expert_keep_rate': 1,
    'expert_use_bn': False,

    'gate_hidden_units': [64],
    'gate_keep_rate': 1,
    'gate_use_bn': False,

    'feature_dims': 18,
    'num_tasks': 2,

    'general_expert_nums': 1,
    'domain_expert_nums': 1,
    'learner_expert_nums': 1,
    'num_learners': 2
}

tower_set = {
    'tower_hidden_units': [64, 32],
    'tower_keep_rate': 1,
    'tower_use_bn': False
}

loss_set = {
    'task_relation': 'product-ctcvr',
    'stop_gd': False,
    't1_weight': 1.0,
    't2_weight': 1.0,
    't1_kd_weight': 0.1,
    't2_kd_weight': 0.1
}

lr = 1e-3
epochs = 1
seed = 3047

train_datapath = "data_sample/ctr_cvr.train"
dev_datapath = "data_sample/ctr_cvr.dev"
test_datapath = "data_sample/ctr_cvr.test"

batchsize = 2048
embedding_size = 8
