# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import numpy as np
import mindspore as ms
from model import TAML
import model_config as cfg

if __name__ == '__main__':
    enum_path = 'data/ctrcvr_enum.pkl'
    feats_columns = [
        '101',
        '121',
        '122',
        '124',
        '125',
        '126',
        '127',
        '128',
        '129',
        '205',
        '206',
        '207',
        '216',
        '508',
        '509',
        '702',
        '853',
        '301']
    voc = joblib.load(enum_path)
    feats_conf = [len(voc[feat]) + 1 for feat in feats_columns]
    model = TAML(cfg, feats_conf)
    input_tensor = ms.Tensor(np.ones([1024, 18]).astype(np.int32))
    input_label_ctr = ms.Tensor(np.ones([1024]).astype(np.float32))
    input_label_ctcvr = ms.Tensor(np.ones([1024]).astype(np.float32))
    param_dict = ms.load_checkpoint("./TAML.ckpt")
    ms.load_param_into_net(model, param_dict)
    ms.export(net=model, inputs=(input_tensor, input_label_ctr, input_label_ctcvr), file_name='./TAML',
              file_format="MINDIR")
