# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import re
import mindspore.nn as nn


def get_optimizer(p, name, params):
    if name == "Adam":
        return nn.Adam(params=p, learning_rate=params["lr"], weight_decay=params["l2_reg"])
    return 0


def parse_float_arg(Input, prefix):
    p = re.compile(prefix+"_[+-]?([0-9]*[.])?[0-9]+")
    m = p.search(Input)
    if m is None:
        return None
    Input = m.group()
    p = re.compile("[+-]?([0-9]*[.])?[0-9]+")
    m = p.search(Input)
    return float(m.group())
