# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import pickle
import json

import numpy as np
import mindspore.dataset as ds


class Dataset():
    def __init__(self, args, data_path, is_clk=False):
        self.args = args
        self.max_hist_len = args.max_hist_len
        with open(args.stat_dir, 'r') as fout:
            stat = json.load(fout)

        self.list_num = stat.get('list_num', 0)
        self.list_len = stat.get('list_len', 0)
        self.itm_spar_fnum = stat.get('itm_spar_fnum', 0)
        self.itm_dens_fnum = stat.get('itm_dens_fnum', 0)
        self.usr_fnum = stat.get('usr_fnum', 0)
        self.hist_fnum = stat.get('hist_fnum', 0)
        self.is_clk = is_clk
        self.shuffle = False

        self.data_path = data_path
        self.data = self.load_data_from_file(data_path)

    def load_data_from_file(self, file_path):
        if self.is_clk:
            uid, spar_ft, dens_ft, lbs, clks, hist, hist_len, div_dicts = pickle.load(open(file_path, 'rb'))
        else:
            uid, spar_ft, dens_ft, lbs, hist, hist_len, div_dicts = pickle.load(open(file_path, 'rb'))
        usr_ft = np.array(uid).reshape([-1, self.usr_fnum])
        spar_ft = np.array(spar_ft).reshape([-1, self.list_num, self.list_len, self.itm_spar_fnum])
        dens_ft = np.array(dens_ft, dtype=np.float32).reshape([-1, self.list_num, self.list_len, self.itm_dens_fnum])
        lbs = np.array(lbs).reshape([-1, self.list_num, self.list_len])
        print('lbs', lbs.shape, lbs.dtype, 'spar ft', spar_ft.shape, 'dens ft', dens_ft.shape)

        data_num = lbs.shape[0]

        usr_ft = usr_ft[:data_num]
        spar_ft = spar_ft[:data_num]
        dens_ft = dens_ft[:data_num]
        if self.is_clk:
            clks = np.array(clks).reshape([-1, self.list_num, self.list_len])[: data_num]
            print('clk', clks.shape)
            print('average click per page', np.sum(clks)/data_num)
            print('average lb per page', np.sum(lbs)/data_num)

        length = np.ones([data_num, self.list_num], dtype=np.int32) * self.list_len
        mask = np.ones([data_num, self.list_num, self.list_len])

        hist_list = []
        print('hist num', len(hist))
        hist_len = list(hist_len)
        if self.max_hist_len:
            for i, lst in enumerate(hist[:data_num]):
                if len(lst) >= self.max_hist_len:
                    new_hist = lst[-self.max_hist_len:]
                    hist_len[i] = self.max_hist_len
                    new_hist = np.array(new_hist)
                else:
                    lst = np.array(lst)
                    new_hist = np.pad(lst, (0, self.max_hist_len - len(lst)), 'constant')
                hist_list.append(new_hist)
            hist_list = np.reshape(np.array(hist_list), [-1, self.max_hist_len, self.hist_fnum])
            hist_len = np.array(hist_len)

        data = {
            'usr_ft': np.array(usr_ft, dtype=np.int32),
            'mask': np.array(mask),
            'lb': np.array(lbs, dtype=np.int32),
            'spar_ft': np.array(spar_ft, dtype=np.int32),
            'dens_ft': np.array(dens_ft, dtype=np.float32),
            'hist_ft': np.array(hist_list, dtype=np.int32),
            'hist_len': np.array(hist_len, dtype=np.int32),
            'len': np.array(length, dtype=np.int32),
            'div_dict': div_dicts,
        }
        if self.is_clk:
            data['clk'] = np.array(clks, dtype=np.float32)
        return data

    def create_ms_dataset(self, batch_size=None, shuffle=False):
        self.shuffle = shuffle
        if batch_size is None:
            batch_size = self.args.batch_size
        dataset = ds.GeneratorDataset(self.ms_dataset_iterator,
                                      column_names=['usr_ft', 'spar_ft', 'dens_ft', 'hist_ft', 'len', 'lb', 'clk'])
        dataset = dataset.batch(batch_size)
        return dataset

    def ms_dataset_iterator(self):
        data_size = self.data.get('usr_ft', None).shape[0]
        indices = np.arange(data_size // self.args.batch_size * self.args.batch_size)
        if self.shuffle:
            np.random.shuffle(indices)
        indices = indices.tolist()
        for _indices in indices:
            data = [self.data.get('usr_ft')[_indices], self.data.get('spar_ft')[_indices],
                    self.data.get('dens_ft')[_indices], self.data.get('hist_ft')[_indices],
                    self.data.get('len')[_indices], self.data.get('lb')[_indices],
                    self.data.get('clk')[_indices]]
            yield data
