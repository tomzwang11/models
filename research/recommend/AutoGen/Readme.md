# Note

This is an example code for the cross-domain model in WSDM 2023 "AutoGen: An Automated Dynamic Model Generation Framework for Recommender System.". (Paper can be download from https://dl.acm.org/doi/10.1145/3539597.3570456)

# Dataset

- Movie-Lens-1M (https://grouplens.org/datasets/movielens/1m/)

# Environment Requirements(#contents)

- Hardware（CPU）
    - Prepare hardware environment with CPU  processor.
- Framework
    - MindSpore-1.9.0 (https://www.mindspore.cn/install/en)
- Requirements
  - pandas
  - numpy
  - random
  - mindspre==1.9.0
  - tensorflow
  - h5py
- For more information, please check the resources below：
  - MindSpore Tutorials (https://www.mindspore.cn/tutorials/en/master/index.html)
  - MindSpore Python API (https://www.mindspore.cn/docs/en/master/index.html)

# Quick Start

We have published our final Mindspore Model in `ms_checkpoints`, and you can evaluate our models as follows:

- running on CPU

  ```python
  # run evaluation example
  python main.py
  ```

# Performance

| Parameters          | CPU                               |
|---------------------|-----------------------------------|
| Model Version       | AutoGen                           |
| Resource            | CPU 2.90GHz;16Core;32G Memory     |
| uploaded Date       | 10/12/2023 (month/day/year)       |
| MindSpore Version   | 1.9.0                             |
| Dataset             | Movielens-1M                      |
| Recall@10           | 0.0662                            |
| NDCG@10             | 0.3543                            |
