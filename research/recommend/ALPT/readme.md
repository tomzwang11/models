# 目录

<!-- TOC -->

- [目录](#目录)
- [ALPT](#ALPT)
- [模型架构](#模型架构)
- [数据集](#数据集)
- [环境要求](#环境要求)
- [快速入门](#快速入门)
- [脚本说明](#脚本说明)
- [评估性能](#CRITEO数据集评估性能)

<!-- /TOC -->

# ALPT

代码基于开源[DCN](https://gitee.com/mindspore/models/tree/master/research/recommend/deep_and_cross), 其中数据集处理和模型架构与[原始论文](https://arxiv.org/abs/2212.05735)存在差异，但实验结论相同。

# 模型架构

[DCN](https://arxiv.org/pdf/1708.05123.pdf)包括Embedding and stacking layer，然后是并行的Cross Network和Deep Network，最后是Combination Layer把Cross Network和Deep Network的结果组合得到输出。

# 数据集

使用的数据集：[1] Guo H 、Tang R和Ye Y等人使用的数据集。 DeepFM: A Factorization-Machine based Neural Network for CTR Prediction[J].2017.

# 环境要求

- 硬件（GPU）
    - 使用GPU处理器来搭建硬件环境。
- 框架
    - [MindSpore](https://www.mindspore.cn/install)

# 快速入门

通过官方网站安装MindSpore后，您可以按照如下步骤进行训练和评估：

1. 下载数据集。

请参考[1] Guo H 、Tang R和Ye Y等人使用的数据集。
[1]DeepFM: A Factorization-Machine based Neural Network for CTR Prediction[J].2017.

```bash
mkdir -p data/origin_data && cd data/origin_data
wget DATA_LINK
tar -zxvf dac.tar.gz
```

2. 使用此脚本预处理数据。处理过程可能需要一小时，生成的MindRecord数据存放在data/mindrecord路径下。

```bash
python src/preprocess_data.py  --data_path=./data/ --dense_dim=13 --slot_dim=26 --threshold=100 --train_line_count=45840617 --skip_id_convert=0
```

3. 开始训练。

数据集准备就绪后，即可在GPU上训练和评估模型。

```bash
bash scripts/train.sh
```

# 脚本说明

## 脚本及样例代码

```bash
├── alpt
    ├── README.md                // 相关说明
    ├── scripts
    │   ├──train.sh             // 训练shell脚本
    │   ├──eval.sh              // 评估的shell脚本
    ├── src
    │   ├──dataset.py             // 创建数据集
    │   ├──deepandcross.py          // deepandcross架构
    │   ├──embedding.py          // embedding模块
    │   ├──callback.py            //  定义回调
    │   ├──config.py            // 参数配置
    │   ├──metrics.py            // 定义AUC
    │   ├──preprocess_data.py    // 预处理数据，生成mindrecord文件
    ├── train.py               // 训练脚本
    ├── eval.py               // 评估脚本
```

## 脚本参数

在config.py中可以同时配置训练参数和评估参数。

- 配置GoogleNet和CIFAR-10数据集。

  ```python
  self.emb_type = 'fp32'                         #embedding 类型
  self.device_id = 0                             #用于训练的GPU ID
  self.epochs = 1                                #训练轮数
  self.batch_size = 10000                        #batch size大小
  self.deep_layer_dim = [1024, 1024]             #deep and cross deeplayer层大小
  self.cross_layer_num = 6                       #deep and cross crosslayer层数
  ```

更多配置细节请参考脚本`config.py`。

# CRITEO数据集评估性能

| Emb_type  | AUC            |
| --------- | -------------  |
| fp32      | 0.8036         |
| lpt       | 0.7983         |
| alpt      | 0.8030         |

