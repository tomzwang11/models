# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

import math
import numpy as np
import mindspore as ms
from mindspore import nn
import mindspore.ops as ops
from mindspore.ops import operations as P
from mindspore.ops import composite as C
from mindspore import Tensor, context
from mindspore.common.parameter import Parameter

def uniform_weight(shape):
    norm = np.random.uniform(-0.1, 0.1, shape).astype(np.float32)
    return Tensor(norm)

class Lsq(nn.Cell):
    def __init__(self, num_bits, grad_scale=None):
        super(Lsq, self).__init__()
        self.num_bits = num_bits
        self.Qn = -2 ** (num_bits - 1)
        self.Qp = 2 ** (num_bits - 1) - 1
        if grad_scale is None:
            grad_scale = 1/math.sqrt(16*self.Qp) # 16 is the embedding dimension
        self.grad_scale = Tensor(grad_scale)

    def construct(self, x, alpha):
        q_w = P.Round()(x/alpha)
        output = P.Maximum()(P.Minimum()(q_w, self.Qp), self.Qn) * alpha
        return output

    def bprop(self, x, alpha, output, grad_output):
        q_w = x / alpha
        indicate_small = (q_w < (self.Qn-1e-4)).int()
        indicate_big = (q_w > (self.Qp+1e-4)).int()
        indicate_middle = 1 - indicate_small - indicate_big

        grad_alpha = (indicate_small*self.Qn + indicate_big*self.Qp + indicate_middle*(P.Round()(q_w)-q_w))
        grad_alpha = P.ReduceSum(keep_dims=True)(grad_alpha, -1) * self.grad_scale
        grad_input = indicate_middle * grad_output
        return grad_input, grad_alpha

class Embedding(nn.Cell):
    def __init__(self, vocab_size, embedding_size):
        super(Embedding, self).__init__()
        self.vocab_size = vocab_size
        self.embedding_size = embedding_size
        self.weight = Parameter(uniform_weight([vocab_size, embedding_size]))
        self.shape_flat = (-1,)
        self.gather = P.Gather()
        self.reshape = P.Reshape()
        self.shape = P.Shape()

    def construct(self, input_ids):
        input_shape = self.shape(input_ids)
        flat_ids = self.reshape(input_ids, self.shape_flat)
        output_for_reshape = self.gather(self.weight, flat_ids, 0)
        out_shape = input_shape + (self.embedding_size,)
        output = self.reshape(output_for_reshape, out_shape)

        return output, self.weight.value()

class Embedding_LPT(Embedding):
    def __init__(self, vocab_size, embedding_size, bit=8):
        super(Embedding_LPT, self).__init__(vocab_size, embedding_size)
        self.bit = bit
        self.clipping = Tensor([0.1])
        self.post_quantization()

    def post_quantization(self, round_type='sr'):
        weight = P.Maximum()(P.Minimum()(self.weight, self.clipping), -self.clipping)
        alpha = self.clipping / (2**(self.bit-1)-1)
        if round_type == "sr":
            rand_noise = ms.Tensor(np.random.rand(*weight.shape), dtype=weight.dtype)
            weight = P.Floor()(weight/alpha+rand_noise)*alpha
        elif round_type == "dr":
            weight = P.Round()(weight/alpha)*alpha
        else:
            raise ValueError("Not supported round type")
        self.weight.set_data(weight)

class Embedding_ALPT(Embedding):
    def __init__(self, vocab_size, embedding_size, bit=8, grad_scale=None):
        super(Embedding_ALPT, self).__init__(vocab_size, embedding_size)
        self.bit, self.Qn, self.Qp = bit, -2**(bit-1), 2**(bit-1)-1
        self.order = Parameter(Tensor(1), requires_grad=False)
        self.lsq = Lsq(bit, grad_scale)
        self.alpha = Parameter(uniform_weight([vocab_size, 1]))
        alpha = self.weight.abs().mean(axis=1).reshape((vocab_size, 1))/(self.Qp)**0.5
        self.alpha.set_data(alpha)
        self.post_quantization()
    def construct(self, input_ids):
        input_shape = self.shape(input_ids)
        flat_ids = self.reshape(input_ids, self.shape_flat)

        output_for_reshape = self.gather(self.weight, flat_ids, 0)
        out_shape = input_shape + (self.embedding_size,)
        output = self.reshape(output_for_reshape, out_shape)

        alpha_output_for_reshape = self.gather(self.alpha, flat_ids, 0)
        alpha_out_shape = input_shape + (1,)
        alpha = self.reshape(alpha_output_for_reshape, alpha_out_shape)

        output = self.lsq(output, alpha)
        return output, self.weight.value()

    def post_quantization(self, round_type='sr'):
        alpha = self.alpha
        alpha = ops.Less()(self.alpha.abs(), 1e-7) * 1e-7 + ops.GreaterEqual()(self.alpha.abs(), 1e-7) * self.alpha
        self.alpha.set_data(alpha)
        if round_type == "sr":
            rand_noise = ms.Tensor(np.random.rand(*self.weight.shape), dtype=self.weight.dtype)
            weight = P.Floor()(self.weight/alpha + rand_noise)
        elif round_type == "dr":
            weight = P.Round()(self.weight/alpha)
        else:
            raise ValueError("Not supported round type")
        weight = P.Maximum()(P.Minimum()(weight, self.Qp), self.Qn) * alpha
        self.weight.set_data(weight)

def test_custom_bprop():
    lsq = Lsq(8)
    context.set_context(mode=context.PYNATIVE_MODE, device_target="CPU")
    grad_all = C.GradOperation(get_all=True)
    lsq.bprop_debug = True
    x = uniform_weight((10, 4))
    y = uniform_weight((10, 1)) * 0.01
    ret = grad_all(lsq)(x, y)
    print(ret)


if __name__ == "__main__":
    # emb = Embedding_ALPT(100,10)
    test_custom_bprop()
