# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from itertools import combinations
from mindspore import nn
from mindspore import ops as P
from mindspore.context import ParallelMode
from mindspore.nn import DistributedGradReducer
from mindspore.ops import functional as F
from mindspore import dtype as mstype
from mindspore import Tensor
from mindspore import ParameterTuple
from mindspore.parallel._utils import (_get_device_num, _get_gradients_mean,
                                       _get_parallel_mode)

from .loss import Ctr_Loss



def build_input_features(sparse_features, var_features, max_len=10):
    features = dict()
    start = 0
    for feat_name in sparse_features:
        if feat_name in features:
            raise ValueError("same feat name")
        features[feat_name] = (start, start + 1)
        start += 1

    for feat_name in var_features:
        if feat_name in features:
            raise ValueError("same feat name")
        features[feat_name] = (start, start + max_len)
        start += max_len
    return features


def generate_pairs(ranges=range(1, 100)):
    rows = []
    cols = []
    for pair in list(combinations(ranges, 2)):
        rows.append(pair[0])
        cols.append(pair[1])
    return Tensor(rows), Tensor(cols)


class MetaNet(nn.Cell):
    def __init__(self, meta_dnn_hidden_units=(32, 64, 32)):
        super(MetaNet, self).__init__()
        self.meta_dnn_hidden_units = meta_dnn_hidden_units
        self.ffn_act_func = nn.ReLU()

    def construct(self, x, mlp_params=None):
        weight_list = []
        bias_list = []
        offset = 0
        for i in range(len(self.meta_dnn_hidden_units) - 1):
            domain_weight = mlp_params[:,
                            offset:offset + self.meta_dnn_hidden_units[i] * self.meta_dnn_hidden_units[i + 1]].reshape(
                -1, self.meta_dnn_hidden_units[i], self.meta_dnn_hidden_units[i + 1])
            offset += self.meta_dnn_hidden_units[i] * self.meta_dnn_hidden_units[i + 1]
            weight_list.append(domain_weight)

        bias_list = [0.0] * len(weight_list)

        for i in range(len(weight_list)):
            x = P.Einsum('ijk,ikl->ijl')((x, weight_list[i])) + bias_list[i]
            if i < len(weight_list) - 1:
                x = self.ffn_act_func(x)

        return x


class Embedding(nn.Cell):
    '''
    Embeddings of uid,mid,cat
    '''

    def __init__(self, sparse_features, var_features, data_max, embedding_size):
        super(Embedding, self).__init__()
        self.embedding_dict = {}
        self.embedding_list = nn.CellList([])
        self.sparse_features = sparse_features
        self.var_features = var_features
        for feat in sparse_features + var_features:
            self.embedding_list.append(nn.Embedding(vocab_size=data_max[feat], embedding_size=embedding_size,
                                                    use_one_hot=False, embedding_table='xavier_uniform'))
            self.embedding_dict[feat] = self.embedding_list[-1]

    def construct(self, value_batch, max_len=10, feat_name=None):
        if feat_name is None:
            flag = 0
            sparse_embedding_list = []
            for feat in self.sparse_features:
                sparse_embedding_list.append(self.embedding_dict[feat](value_batch[:, flag]))
                flag += 1
            varlen_sparse_embedding_list = []
            for feat in self.var_features:
                varlen_sparse_embedding_list.append(
                    P.reduce_mean(self.embedding_dict[feat](value_batch[:, flag:flag + max_len]), 1))
                flag += max_len
            return sparse_embedding_list, varlen_sparse_embedding_list
        return self.embedding_dict[feat_name](value_batch)


class DfubAttention(nn.Cell):
    def __init__(self, embedding_size, internal_size, num_head=4, use_res=True):
        super(DfubAttention, self).__init__()
        self.num_head = num_head
        self.internal_size = internal_size
        self.embedding_size = embedding_size

        self.key_dense = nn.Dense(embedding_size, internal_size, has_bias=False)
        self.query_dense = nn.Dense(embedding_size, internal_size, has_bias=False)
        self.value_dense = nn.Dense(embedding_size, internal_size, has_bias=False)
        self.value_dense_post = nn.Dense(internal_size, embedding_size, has_bias=False)

        self.use_res = use_res
        if self.use_res:
            self.res_dense = nn.Dense(embedding_size, embedding_size, has_bias=False)

    def construct(self, seqs, q_tar_embedding, k_tar_embedding, v_tar_embedding):
        queries = self.query_dense(seqs)
        keys = self.key_dense(seqs)
        values = self.value_dense(seqs)

        q_tar_embedding = P.Reshape()(q_tar_embedding, (-1, self.internal_size, self.embedding_size))
        queries = P.bmm(queries, q_tar_embedding)
        k_tar_embedding = P.Reshape()(k_tar_embedding, (-1, self.internal_size, self.embedding_size))
        keys = P.bmm(keys, k_tar_embedding)
        if v_tar_embedding is not None:
            v_tar_embedding = P.Reshape()(v_tar_embedding, (-1, self.internal_size, self.embedding_size))
            values = P.bmm(values, v_tar_embedding)
        else:
            values = self.value_dense_post(values)

        q_ = P.Concat(axis=0)(P.Split(axis=2, output_num=self.num_head)(queries))
        k_ = P.Concat(axis=0)(P.Split(axis=2, output_num=self.num_head)(keys))
        v_ = P.Concat(axis=0)(P.Split(axis=2, output_num=self.num_head)(values))

        outputs = P.BatchMatMul()(q_, P.Transpose()(k_, (0, 2, 1)))
        normalized_att_scores = P.Softmax()(outputs)
        outputs = P.BatchMatMul()(normalized_att_scores, v_)
        outputs = P.Concat(axis=2)(P.Split(axis=0, output_num=self.num_head)(outputs))  # batch, seq_len, emb_size
        if self.use_res:
            outputs += self.res_dense(seqs)
        outputs = nn.ReLU()(outputs)
        return outputs


class DFFM(nn.Cell):
    '''
    DFFM
    '''

    def __init__(self, sparse_features, var_features, domain_index, data_max, embedding_size,
                 max_len=10, add_dffi=True, add_dfub=True, hidden_unit=128,
                 hidden_depth=4,
                 history_feature_list=None, history_seq_list=None, domain_feature_list=None, internal_size=8,
                 use_reshape_value=True, l2_reg=0.0):
        super(DFFM, self).__init__()
        self.embedding_size = embedding_size
        self.n_fields = len(sparse_features + var_features)
        self.sparse_features = sparse_features
        self.var_features = var_features
        self.add_dffi = add_dffi
        self.add_dfub = add_dfub
        self.max_len = max_len
        self.use_reshape_value = use_reshape_value
        self.l2_reg = l2_reg
        meta_dnn_hidden_units = [16, 16]

        self.feature_index = build_input_features(sparse_features, var_features)
        self.domain_feature_list = domain_feature_list

        if self.add_dffi:
            self.domain_index = domain_index
            meta_dnn_hidden_units = [self.embedding_size] + meta_dnn_hidden_units
            meta_param_size = sum([meta_dnn_hidden_units[i] * meta_dnn_hidden_units[i + 1] for i in
                                   range(len(meta_dnn_hidden_units) - 1)])
            self.domain_map_dnn = nn.Dense(in_channels=embedding_size, out_channels=meta_param_size,
                                           weight_init='xavier_uniform', activation='relu')
            self.meta_net = MetaNet(meta_dnn_hidden_units=meta_dnn_hidden_units)
            self.domain_embeddinglayer = Embedding(domain_feature_list, [], data_max, embedding_size)

        if self.add_dfub:
            self.history_feature_list = history_feature_list
            self.history_seq_list = history_seq_list
            self.dfub_embedding_size = embedding_size * internal_size
            self.dfub_embeddinglayer1 = Embedding(domain_feature_list + history_feature_list, [], data_max,
                                                  embedding_size * internal_size)
            self.dfub_embeddinglayer2 = Embedding([], history_seq_list, data_max, embedding_size)
            self.dfub_layer = nn.CellList([DfubAttention(embedding_size, internal_size)
                                           for _ in range(len(self.history_feature_list))])

        self.embeddinglayer = Embedding(sparse_features, var_features, data_max, embedding_size)

        input_size = int(self.n_fields * embedding_size + (self.n_fields - 1) * self.n_fields / 2)
        if self.add_dffi:
            input_size += self.n_fields * meta_dnn_hidden_units[-1]
        if self.add_dfub:
            input_size += embedding_size * len(history_feature_list)
        self.dense_list = nn.CellList([])
        self.hidden_depth = hidden_depth
        for i in range(hidden_depth):
            if i == 0:
                self.dense_list.append(nn.Dense(in_channels=input_size, out_channels=hidden_unit,
                                                weight_init='xavier_uniform', activation='relu'))
            elif i == hidden_depth - 1:
                self.dense_list.append(nn.Dense(in_channels=hidden_unit, out_channels=1,
                                                weight_init='xavier_uniform'))
            else:
                self.dense_list.append(nn.Dense(in_channels=hidden_unit, out_channels=hidden_unit,
                                                weight_init='xavier_uniform'))

    def meta_transformation(self, X, fm_input):
        # generate domain embedding
        domain_emb_list = self.embedding_lookup(X=X, embeddinglayer=self.domain_embeddinglayer,
                                                feature_names=self.domain_feature_list)
        domain_emb = domain_emb_list[0]
        domain_emb = P.Squeeze(axis=1)(domain_emb)
        domain_emb = F.relu(domain_emb)
        domain_vec = self.domain_map_dnn(domain_emb)
        fm_input = self.meta_net(fm_input, domain_vec)
        return fm_input

    def embedding_lookup(self, X, embeddinglayer, feature_names):
        emb_list = []
        for fc in feature_names:
            lookup_idx = self.feature_index[fc]
            input_tensor = X[:, lookup_idx[0]:lookup_idx[1]]
            emb = embeddinglayer(input_tensor, self.max_len, feat_name=fc)
            emb_list.append(emb)
        return emb_list

    def construct(self, value_batch):
        all_embedding_list = []
        sparse_embedding_list, varlen_sparse_embedding_list = self.embeddinglayer(value_batch, self.max_len)
        rows, cols = generate_pairs(range(self.n_fields))
        sparse_input = P.Stack(1)(sparse_embedding_list + varlen_sparse_embedding_list)
        batch_size = sparse_input.shape[0]
        all_embedding_list.append(sparse_input)
        if self.add_dffi:
            i_all_embeddings = self.meta_transformation(value_batch, sparse_input)
            i_all_embeddings = P.Concat(2)((i_all_embeddings, sparse_input))
        else:
            i_all_embeddings = sparse_input
        if self.add_dfub:
            dfub_output = []
            target_emb_list = self.embedding_lookup(X=value_batch, embeddinglayer=self.dfub_embeddinglayer1,
                                                    feature_names=self.history_feature_list)
            his_emb_list = self.embedding_lookup(value_batch, self.dfub_embeddinglayer2, self.history_seq_list)
            cat_domain_emb_list = self.embedding_lookup(value_batch, self.dfub_embeddinglayer1,
                                                        self.domain_feature_list)
            all_embedding_list.extend(target_emb_list)
            all_embedding_list.extend(his_emb_list)
            all_embedding_list.extend(cat_domain_emb_list)

            for i in range(len(target_emb_list)):
                part_target_emb = target_emb_list[i]
                his_emb = his_emb_list[i]
                cat_domain_emb = cat_domain_emb_list[0]
                part1_num = int(0.5 * self.dfub_embedding_size)
                part2_num = self.dfub_embedding_size - part1_num
                target_emb = P.Concat(axis=-1)((part_target_emb[:, :, :part1_num], cat_domain_emb[:, :, :part2_num]))
                if self.use_reshape_value:
                    his_emb_new = self.dfub_layer[i](his_emb, target_emb, target_emb, target_emb)
                else:
                    his_emb_new = self.dfub_layer[i](his_emb, target_emb, target_emb, None)
                his_emb_new = P.ReduceSum()(his_emb_new, axis=1)
                dfub_output.append(his_emb_new)
            dfub_output_emb = P.Concat(axis=-1)(dfub_output)

        i_all_embeddings_t = P.Transpose()(i_all_embeddings, (1, 0, 2))
        i_left = P.Transpose()(P.gather(i_all_embeddings_t, rows, 0), (1, 0, 2))
        i_right = P.Transpose()(P.gather(i_all_embeddings_t, cols, 0), (1, 0, 2))
        i_level_2_matrix = P.reduce_sum(i_left * i_right, 2)

        d_layer_input = P.Concat(-1)((i_level_2_matrix, P.Flatten()(i_all_embeddings)))
        if self.add_dfub:
            d_layer_input = P.Concat(-1)((d_layer_input, dfub_output_emb))
        d_layer_output = d_layer_input

        for i in range(self.hidden_depth):
            d_layer_output = self.dense_list[i](d_layer_output)

        d_layer_output = P.Reshape()(d_layer_output, (-1,))
        self.final = d_layer_output
        self.outputs = P.Sigmoid()(self.final)
        for i in range(len(all_embedding_list)):
            all_embedding_list[i] = P.Reshape()(all_embedding_list[i], (batch_size, -1))
        l2_all_embedding_list = P.Concat(axis=-1)(all_embedding_list)
        self.l2_loss = self.l2_reg * P.ReduceMean()(l2_all_embedding_list)
        return self.final, self.outputs, self.l2_loss


class Accuracy(nn.Cell):
    def __init__(self):
        super(Accuracy, self).__init__()
        self.reduce_mean = P.ReduceMean()
        self.cast = P.Cast()
        self.equal = P.Equal()
        self.round = P.Round()

    def construct(self, y_hat, target):
        accuracy = self.reduce_mean(self.cast(self.equal(self.round(y_hat), target), mstype.float32))
        return accuracy


class CustomWithLossCell(nn.Cell):
    def __init__(self, backbone, loss_fn):
        super(CustomWithLossCell, self).__init__(auto_prefix=False)
        self._backbone = backbone
        self._loss_fn = loss_fn
        self.loss = P.SigmoidCrossEntropyWithLogits()
        self.ReduceMean_false = P.ReduceMean(keep_dims=False)

    def construct(self, value_batch, label_batch):
        logits, _, l2_loss = self._backbone(value_batch)
        log_loss = self.loss(logits, label_batch) + l2_loss
        mean_log_loss = self.ReduceMean_false(log_loss)
        return mean_log_loss


class TrainOneStepCell(nn.Cell):
    def __init__(self, network, optimizer):
        super(TrainOneStepCell, self).__init__(auto_prefix=False)
        self.network = network
        self.weights = ParameterTuple(network.trainable_params())
        self.optimizer = optimizer
        self.grad = P.GradOperation(get_by_list=True)
        self.loss = Ctr_Loss()
        self.reduce_flag = False
        self.grad_reducer = F.identity
        self.parallel_mode = _get_parallel_mode()
        self.reducer_flag = self.parallel_mode in (ParallelMode.DATA_PARALLEL, ParallelMode.HYBRID_PARALLEL)
        if self.reducer_flag:
            self.mean = _get_gradients_mean()
            self.degree = _get_device_num()
            self.grad_reducer = DistributedGradReducer(self.weights, self.mean, self.degree)

    def construct(self, value_batch, label_batch):
        weights = self.weights
        loss = self.network(value_batch, label_batch)
        grads = self.grad(self.network, weights)(value_batch, label_batch)
        grads = self.grad_reducer(grads)
        loss = F.depend(loss, self.optimizer(grads))
        return loss
