# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the License);
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an AS IS BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
import os
import mindspore as ms

from data_generator import Dataset
from algorithm import Model
from config import default_config

os.environ['GLOG_v'] = '4'

if __name__ == "__main__":
    ms.set_context(mode=ms.context.PYNATIVE_MODE, device_target="CPU")

    defined_config = default_config()
    model = Model(config=defined_config)
    dataset = Dataset(trainset_path="./data/train_data.pkl", testset_path="./data/test_data.pkl",
                      candidate_actions_set=defined_config.default_action)
    optimizer = ms.nn.Adam(model.trainable_params(), learning_rate=1e-3)
    model.set_train()


    def forward_fn(batch):
        other_info, history, news_candidate, video_candidate, action, reward, \
        next_news_candidate, next_video_candidate, news_num, video_num, next_news_num, next_video_num, action_mask = \
            batch
        batch = {"user_info": ms.Tensor(other_info[:, 0:1]),
                 "context_info": ms.Tensor(ms.ops.concat([other_info[:, 1:], history], axis=1)),
                 "video_list": ms.Tensor(video_candidate),
                 "video_num": ms.Tensor(video_num),
                 "news_list": ms.Tensor(news_candidate),
                 "news_num": ms.Tensor(news_num),
                 "next_user_info": ms.Tensor(other_info[:, 0:1]),
                 "next_context_info": ms.Tensor(ms.ops.concat([other_info[:, 1:], history], axis=1)),
                 "next_video_list": ms.Tensor(next_video_candidate),
                 "next_video_num": ms.Tensor(next_video_num),
                 "next_news_list": ms.Tensor(next_news_candidate),
                 "next_news_num": ms.Tensor(next_news_num),
                 "action": ms.Tensor(action)}

        q, target_q = model(batch)

        # calculate loss
        reward_decay = ms.Tensor(defined_config.reward_decay_array)
        reward_sum = ms.Tensor(ms.Tensor.sum(reward * reward_decay, axis=1))
        q = ms.ops.reshape(q, (-1, defined_config.action_set_size))
        target_q = ms.ops.reshape(
            target_q, (-1, defined_config.action_set_size))
        _, max_q = ms.ops.max(target_q, axis=1)
        action_q = ms.Tensor.sum(action_mask * q, axis=1)
        target = reward_sum + defined_config.gamma * max_q
        loss = ms.ops.mean(ms.ops.square(target - action_q))

        return loss


    grad_fn = ms.value_and_grad(
        forward_fn, None, optimizer.parameters, has_aux=False)

    for i in range(200):
        other_info, history, news_candidate, video_candidate, action, reward, next_news_candidate, \
        next_video_candidate, news_num, video_num, next_news_num, next_video_num, action_mask = \
            dataset.sample_trainset(256)
        batch = [ms.Tensor(other_info),
                 ms.Tensor(history),
                 ms.Tensor(news_candidate),
                 ms.Tensor(video_candidate),
                 ms.Tensor(action),
                 ms.Tensor(reward),
                 ms.Tensor(next_news_candidate),
                 ms.Tensor(next_video_candidate),
                 ms.Tensor(news_num),
                 ms.Tensor(video_num),
                 ms.Tensor(next_news_num),
                 ms.Tensor(next_video_num),
                 ms.Tensor(action_mask)]

        loss, grads = grad_fn(batch)
        loss = ms.ops.depend(loss, optimizer(grads))
        print(loss)
