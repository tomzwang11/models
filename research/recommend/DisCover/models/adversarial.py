# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
from mindspore import nn
from .basic_cell import BasicCell


class Discriminator(BasicCell):
    def __init__(self, latent_dim=300):
        super(Discriminator, self).__init__()
        self.model = nn.SequentialCell(
            nn.Dense(latent_dim, 512),
            nn.BatchNorm1d(512),
            nn.LeakyReLU(0.2),
            nn.Dense(512, 256),
            nn.BatchNorm1d(256),
            nn.Dropout(0.4),
            nn.LeakyReLU(0.2),
            nn.Dense(256, 128),
            nn.BatchNorm1d(128),
            nn.Dropout(0.4),
            nn.LeakyReLU(0.2),
            nn.Dense(128, 1),
            nn.Sigmoid(),
        )

    def construct(self, feature):
        d_in = feature
        validity = self.model(d_in)
        return validity.squeeze()
