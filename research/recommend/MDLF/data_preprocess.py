# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Generate data in mindrecord format."""
import os
import random
from collections import defaultdict

import pandas as pd
import numpy as np
from mindspore.mindrecord import FileWriter

from src.model_utils.config import config

random.seed(123)


def generate_criteo_landscape():
    data_file = os.path.join(config.source_data_path, 'criteo_attribution_dataset.tsv.gz')
    print('Preprocessing source file...')
    df = pd.read_csv(data_file, sep='\t', compression='gzip')
    df['day'] = np.floor(df.timestamp / 86400.).astype(int)
    start_test_day = 25
    features = ['cat1', 'cat4', 'cat6', 'cat8', 'cat9']
    feat2id = {}
    idx = 0
    for feat in features:
        feats = df[feat].unique()
        for f in feats:
            key = feat + '_' + str(f)
            feat2id[key] = idx
            idx += 1

    def generate_landscape(df, path):
        def cat2id(x, name_str):
            return feat2id[name_str + '_' + str(x)]

        groups = df.groupby(['cat1', 'cat4', 'cat6', 'cat8', 'cat9'])
        MINDRECORD_FILE = os.path.join(config.mindrecord_path, path)
        writer = FileWriter(file_name=MINDRECORD_FILE, shard_num=1)
        schema = {"data": {"type": "int32", "shape": [-1]},
                  "position_labels": {"type": "int32", "shape": [-1]},
                  "score_labels": {"type": "float32", "shape": [-1]}}
        writer.add_schema(schema, "it is a preprocessed landscape dataset")
        cnt = 0
        for name, group in groups:
            if len(group) < 3:
                continue
            times = len(group) // 10 + 1
            k = min(10, len(group))
            feat_index = np.array([cat2id(name[0], 'cat1'),
                                   cat2id(name[1], 'cat4'),
                                   cat2id(name[2], 'cat6'),
                                   cat2id(name[3], 'cat8'),
                                   cat2id(name[4], 'cat9')])
            for _ in range(times):
                res = sorted(group['cost'].sample(k).to_list(), reverse=True)
                idx = random.sample(range(k), 3)
                for i in idx:
                    cnt += 1
                    sample = {"data": feat_index,
                              "position_labels": np.array([i]),
                              "score_labels": np.array([res[i]])}
                    if cnt % 10000 == 0:
                        print(f'write {cnt} lines.')
                    writer.write_raw_data([sample])
        writer.commit()

    df_train = df[df['day'] < start_test_day]
    df_test = df[df['day'] >= start_test_day]
    print('Generating train set...')
    generate_landscape(df_train, 'landscape_criteo_train.mindrecord')
    print('Generating test set...')
    generate_landscape(df_test, 'landscape_criteo_test.mindrecord')


def generate_ipinyou_landscape():
    oses = ["windows", "ios", "mac", "android", "linux"]
    browsers = ["chrome", "sogou", "maxthon", "safari", "firefox", "theworld", "opera", "ie"]
    f1sp = ["useragent", "slotprice"]

    list_feat = ["weekday", 'hour', "slotwidth", "slotheight", "slotvisibility", "slotprice"]

    # log.column:
    # click  weekday  hour  bidid  timestamp  logtype  ipinyouid  useragent  IP  region  city
    # adexchange  domain  url  urlid  slotid  slotwidth  slotheight  slotvisibility  slotformat  slotprice  creative
    # bidprice  payprice  keypage  advertiser  usertag

    def featTrans(name, content):
        content = content.lower()
        if name == "useragent":
            operation = "other"
            for o in oses:
                if o in content:
                    operation = o
                    break
            browser = "other"
            for b in browsers:
                if b in content:
                    browser = b
                    break
            return operation + "_" + browser
        if name == "slotprice":
            price = int(content)
            if price > 100:
                return "101+"
            if price > 50:
                return "51-100"
            if price > 10:
                return "11-50"
            if price > 0:
                return "1-10"
            return "0"
        return ""

    def getFeats():
        # initialize
        namecol = {}
        list_featindex = {}
        featindex = {}
        max_list_index = 0
        maxindex = 0
        fi = open(os.path.join(config.source_data_path, 'all', 'train.log.txt'), 'r')
        first = True
        n = 0
        for line in fi:
            s = line.split('\t')
            if first:
                first = False
                for i in range(0, len(s)):
                    namecol[s[i].strip()] = i
                    if i > 0 and s[i] in list_feat:
                        featindex[str(i) + ':other'] = maxindex
                        maxindex += 1
                continue
            feats = ''
            for f in list_feat:
                col = namecol[f]
                if f in f1sp:
                    content = featTrans(f, s[col])
                else:
                    content = s[col]
                feat = str(col) + ':' + content
                if feat not in featindex:
                    featindex[feat] = maxindex
                    maxindex += 1
                feats += feat + '\t'
            if feats not in list_featindex:
                list_featindex[feats] = max_list_index
                max_list_index += 1
            n += 1
        print(n, max_list_index)
        print('feature size: ' + str(maxindex))
        print('list feature size: ' + str(max_list_index))
        print('Total line: ' + str(n))
        return namecol, list_featindex, featindex

    def groupDataFile(list_featindex, mode):
        namecol = {}
        first = True
        featsbucket = defaultdict(list)
        fi = open(os.path.join(config.source_data_path, 'all', '%s.log.txt' % mode), 'r')
        for line in fi:
            s = line.split('\t')
            if first:
                first = False
                for i in range(0, len(s)):
                    namecol[s[i].strip()] = i
                continue
            feats = ''
            for f in list_feat:
                col = namecol[f]
                if f in f1sp:
                    content = featTrans(f, s[col])
                else:
                    content = s[col]
                feat = str(col) + ':' + content
                feats += feat + '\t'
            if feats not in list_featindex:
                continue
            featsbucket[feats].append(line)
        return featsbucket

    def generate_landscape(namecol, featindex, list_featindex, mode):
        featsbucket = groupDataFile(list_featindex, mode)
        print('Group file done.')
        MINDRECORD_FILE = os.path.join(config.mindrecord_path, 'landscape_ipinyou_%s.mindrecord' % mode)
        writer = FileWriter(file_name=MINDRECORD_FILE, shard_num=1)
        schema = {"data": {"type": "int32", "shape": [-1]},
                  "position_labels": {"type": "int32", "shape": [-1]},
                  "score_labels": {"type": "int32", "shape": [-1]}}
        writer.add_schema(schema, "it is a preprocessed landscape dataset")
        cnt = 0
        for key in featsbucket:
            feats = key.split('\t')[:-1]
            data = []
            for feat in feats:
                if feat not in featindex:
                    feat = feat.split(':')[0] + ':other'
                data.append(featindex[feat])
            times = len(featsbucket[key]) // 10
            for _ in range(times):
                lists = random.sample(featsbucket[key], min(10, len(featsbucket)))
                lists = sorted(lists, key=lambda x: -float(x.split('\t')[23]))
                idxs = random.sample(range(10), 5)
                for idx in idxs:
                    cnt += 1
                    line = lists[idx]
                    s = line.split('\t')

                    sample = {"data": np.array(data),
                              "position_labels": np.array([idx]),
                              "score_labels": np.array(int(s[namecol['payprice']]))}
                    if cnt % 10000 == 0:
                        print(f'write {cnt} lines.')
                    writer.write_raw_data([sample])
        writer.commit()

    print('Preprocessing source file...')
    namecol, list_featindex, featindex = getFeats()
    print('Generating train set...')
    generate_landscape(namecol, featindex, list_featindex, 'train')
    print('Generating test set...')
    generate_landscape(namecol, featindex, list_featindex, 'test')


if __name__ == '__main__':
    os.makedirs(config.mindrecord_path, exist_ok=True)
    if config.dataset == 'criteo':
        generate_criteo_landscape()
    elif config.dataset == 'ipinyou':
        generate_ipinyou_landscape()
    else:
        raise NotImplementedError
