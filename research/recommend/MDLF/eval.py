# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
################# eval MDLF example########################
python eval.py
"""
from mindspore import load_checkpoint
from mindspore import load_param_into_net
from mindspore import context
from mindspore.communication.management import init
from mindspore.context import ParallelMode
from mindspore.train.model import Model
from mindspore.common import set_seed
from src.model_utils.config import config
from src.model_utils.device_adapter import get_device_id, get_device_num, get_rank_id, get_job_id
from src.load_dataset import create_dataset
from src.model_builder import ModelBuilder
from src.MDLFMetric import MDLFMetric


set_seed(456)


def run_eval():
    print('device id:', get_device_id())
    print('device num:', get_device_num())
    print('rank id:', get_rank_id())
    print('job id:', get_job_id())

    device_target = config.device_target
    context.set_context(mode=context.GRAPH_MODE,
                        device_target=config.device_target)
    context.set_context(save_graphs=False)

    device_num = get_device_num()

    if config.run_distribute:
        context.reset_auto_parallel_context()
        context.set_auto_parallel_context(device_num=device_num,
                                          parallel_mode=ParallelMode.DATA_PARALLEL, gradients_mean=True)
        if device_target == "Ascend":
            context.set_context(device_id=get_device_id())
            init()
        elif device_target == "GPU":
            init()

    if config.run_distribute:
        context.reset_auto_parallel_context()
        context.set_auto_parallel_context(device_num=device_num,
                                          parallel_mode=ParallelMode.DATA_PARALLEL, gradients_mean=True)
        if device_target == "Ascend":
            context.set_context(device_id=get_device_id())
            init()
        elif device_target == "GPU":
            init()
    else:
        context.set_context(device_id=get_device_id())
    print("init finished.")

    ds_eval = create_dataset(config.mindrecord_path, config.dataset, config.batch_size,
                             training=False, target=config.device_target)

    model_builder = ModelBuilder(config, 1, dump_config=False)
    train_net, eval_net = model_builder.get_train_eval_net()
    train_net.set_train()
    eval_net.set_train(False)
    metric = MDLFMetric(config.intervals, config.length, config.interval_length)
    model = Model(train_net, eval_network=eval_net, metrics={'MDLFMetric': metric})
    param_dict = load_checkpoint(config.ckpt_path)
    load_param_into_net(eval_net, param_dict)
    res = model.eval(ds_eval, dataset_sink_mode=False)
    ret = ''
    for key, value in res['MDLFMetric'].items():
        ret += '    {:15s}: {}\n'.format(str(key), value)
    print(ret)


if __name__ == '__main__':
    run_eval()
