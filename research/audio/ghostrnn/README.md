# GhostRNN

Paper Link: [https://www.isca-speech.org/archive/interspeech_2023/zhou23g_interspeech.html](https://www.isca-speech.org/archive/interspeech_2023/zhou23g_interspeech.html)

## Requirements

`conda create -n ghostrnn -file conda.list
`

## Inference

Running using Google Speech Commands dataset which should be extracted into mfcc.  A demo code is as follows:
python inference.py ----checkpoint_url ckpts/ms/ghost_rnn_9547.ckpt-10400.ckpt --data_path data/example_data_feature.npy

## Results

The average accuracy for three checkpoints in the paper is 94.79%, and one of the Confusion Matrix is as follows :
`
INFO: Confusion Matrix:
 [[408   0   0   0   0   0   0   0   0   0   0   0]
 [  0 368   0   6   1   4   4   5  10   2   2   6]
 [  0   5 408   0   0   1   4   0   0   0   0   1]
 [  0   2   1 390   0   3   5   0   0   0   0   4]
 [  0   6   1   0 398   1   0   2   4   8   4   1]
 [  0   3   0   8   1 373   1   3   2   0   6   9]
 [  0   5   7   0   1   0 396   3   0   0   0   0]
 [  0   5   0   0   0   0   5 382   1   1   1   1]
 [  0  12   0   1   4   3   0   0 366  10   0   0]
 [  0   8   1   1  12   0   1   0   4 372   0   3]
 [  0   0   0   0   1   2   0   0   0   0 404   4]
 [  0   3   0  12   2   5   1   0   0   1   1 377]]
INFO: Test accuracy = 94.93% (N=4890)`

## Citation

```text
@inproceedings{zhou23g_interspeech,
  author={Hang Zhou and Xiaoxu Zheng and Yunhe Wang and Michael Bi Mi and Deyi Xiong and Kai Han},
  title={{GhostRNN: Reducing State Redundancy in RNN with Cheap Operations}},
  year=2023,
  booktitle={Proc. INTERSPEECH 2023},
  pages={226--230},
  doi={10.21437/Interspeech.2023-2417}
}
```
