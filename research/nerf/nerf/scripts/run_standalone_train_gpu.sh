#!/bin/bash
# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

if [ $# != 3 ] && [ $# != 4 ]
then
    echo "Usage: bash run_standalone_train_gpu.sh [CONFIG_DIR] [DATASET_PATH] [OUT_DIR] [PRETRAINED_CKPT_PATH](optional)"
    exit 1
fi

get_real_path(){
  if [ "${1:0:1}" == "/" ]; then
    echo "$1"
  else
    echo "$(realpath -m $PWD/$1)"
  fi
}

CONFIG_DIR=$(get_real_path $1)
DATASET_PATH=$(get_real_path $2)
OUT_DIR=$(get_real_path $3)

TRAIN_CONFIG=$CONFIG_DIR/train_config.json
DS_CONFIG=$CONFIG_DIR/data_config.json

if [ $# == 4 ]
then
    PRETRAINED_CKPT_PATH=$(get_real_path $4)
fi

if [ ! -d $DATASET_PATH ]
then
    echo "error: DATASET_PATH=$DATASET_PATH is not a directory"
    exit 1
fi


if [ $# == 4 ] && [ ! -f $PRETRAINED_CKPT_PATH ]
then
    echo "error: PRETRAINED_CKPT_PATH=$PRETRAINED_CKPT_PATH is not a file"
    exit 1
fi

echo "config_dir: " $CONFIG_DIR
echo "dataset_path: " $DATASET_PATH
echo "out_path: " $OUT_DIR
echo "pretrained: " $PRETRAINED_CKPT_PATH

if [ $# == 3 ]
then
  echo "here"
  python train.py --data-path $DATASET_PATH --data-config $DS_CONFIG --train-config $TRAIN_CONFIG \
  --out-dir $OUT_DIR &> standalone_train.log &
fi
if [ $# == 4 ]
then
  python train.py --data-path $DATASET_PATH --data-config $DS_CONFIG --train-config $TRAIN_CONFIG \
  --out-dir $OUT_DIR --init-ckpt $PRETRAINED_CKPT_PATH &> standalone_train.log &
fi
