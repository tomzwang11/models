# Copyright 2023 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Evaluation using the test split of passed dataset."""

import os
import argparse
import csv
import json
import copy
from pathlib import Path

import matplotlib.pyplot as plt

import mindspore as ms
from mindspore import load_checkpoint, load_param_into_net

import numpy as np
import tqdm

from src.volume_rendering import VolumeRendering
from src.data.dataset import LitData
from src.data.ray_utils import batches_to_images
from src.tools.metrics import get_psnr, get_mse

DEFAULT_MODEL_CONFIG = Path('src') / 'configs' / 'nerf_config.json'


def parse_args():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data-path', type=Path, required=True,
                        help='Dataset directory.')
    parser.add_argument('--data-config', type=Path, required=True,
                        help='Path to the dataset config.')
    parser.add_argument('--model-config', type=Path,
                        default=DEFAULT_MODEL_CONFIG,
                        help='Volume rendering model config.')
    parser.add_argument('--model-ckpt', type=Path, required=True,
                        help='Model checkpoints.')
    parser.add_argument('--rays-batch', type=int, default=400,
                        help='Rays in batch size.')
    parser.add_argument('--out-path', type=Path, required=True,
                        help='Directory for output data saving:'
                             'predicted image, configs, ')
    parser.add_argument('--mode', choices=['graph', 'pynative'],
                        default='graph',
                        help='Model representation mode. '
                             'Pynative for debugging.')
    return parser.parse_args()


def main():
    args = parse_args()
    mode = ms.GRAPH_MODE if args.mode == 'graph' else ms.PYNATIVE_MODE
    ms.set_context(mode=mode,
                   device_target='GPU')

    # Load data.
    with open(args.data_config, 'r') as f:
        data_cfg = json.load(f)
    with open(args.model_config, 'r') as f:
        model_cfg = json.load(f)

    # Init dataset.
    test_ds = LitData(data_cfg['data_type'],
                      args.data_path, stage='test',
                      white_bkgd=data_cfg['white_bkgd'],
                      ndc_coord=data_cfg['ndc_coord'],
                      load_radii=data_cfg['load_radii'],
                      use_pixel_centers=data_cfg['use_pixel_centers'],
                      chunk=args.rays_batch,
                      epoch_size=data_cfg['epoch_size']
                      )

    h, w = test_ds.height, test_ds.width
    test_ds_near = test_ds.near
    test_ds_far = test_ds.far
    image_sizes = test_ds.image_sizes
    print('Dataset total chunks', test_ds.total_chunks, 'image size', h, w)

    test_ds = ms.dataset.GeneratorDataset(
        source=test_ds,
        column_names=['rays', 'target'],
        column_types=[ms.float32, ms.float32],
        shuffle=False
    )

    # Init model
    volume_rendering = VolumeRendering(
        **model_cfg,
        near=test_ds_near,
        far=test_ds_far,
        white_bkgr=data_cfg['white_bkgd'],
        perturbation=False,
        ray_shape=data_cfg['ray_shape'],
        stop_level_grad=True
    )
    ckpt = load_checkpoint(str(args.model_ckpt))
    load_param_into_net(volume_rendering, ckpt)

    os.makedirs(args.out_path, exist_ok=True)

    rgb_pred_batches = []
    rgb_target_batches = []
    for _, (rays, target) in tqdm.tqdm(enumerate(test_ds)):

        out = volume_rendering(rays)
        fine_rgb = out[:, 3: 6]

        rgb_pred = np.clip(copy.deepcopy(fine_rgb.asnumpy()), 0, 1)
        rgb_target = np.clip(target.asnumpy(), 0, 1)
        rgb_pred_batches.append(rgb_pred)
        rgb_target_batches.append(rgb_target)

    # Reshape batches to images
    rgb_pred_images = batches_to_images(np.array(rgb_pred_batches), image_sizes)
    rgb_target_images = batches_to_images(np.array(rgb_target_batches), image_sizes)

    # Calculate metrics
    metrics = {}
    for img_idx, (rgb_pred, rgb_target) in enumerate(zip(rgb_pred_images, rgb_target_images)):

        mse = get_mse(rgb_pred, rgb_target)
        p = get_psnr(rgb_pred, rgb_target)
        metrics[img_idx] = {'psnr': p, 'mse': mse, 'img_idx': img_idx}

        # Save images
        plt.imsave(
            args.out_path / f'image_fine_{img_idx}_psnr_{p}.png',
            (rgb_pred * 255).astype(np.uint8)
            )
        plt.imsave(
            args.out_path / f'image_gt_{img_idx}_psnr_{p}.png',
            (rgb_target * 255).astype(np.uint8)
            )

    # Write to csv.
    csv_metrics = args.out_path / 'metrics.csv'
    with open(csv_metrics, 'w', newline='') as csvfile:
        fieldnames = ['img_idx', 'psnr', 'mse']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows([v for v in metrics.values()])
        mean_metric_writer = csv.writer(csvfile)
        psnr = np.mean(np.array([v['psnr'] for v in metrics.values()]))
        mean_metric_writer.writerow([f'Mean_PSNR: {psnr}'])
        print(f'Mean_PSNR: {psnr}')


if __name__ == '__main__':
    main()
